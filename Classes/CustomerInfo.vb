﻿Option Explicit On
Option Strict On

<Serializable()> _
Public Class CustomerInfo

    Private _DeviceIMEI As String
    Private _CustomerID As String
    Private _CustomerName As String
    Private _AppVersion As String

    Public Sub New()

        _DeviceIMEI = ""
        _CustomerID = ""
        _CustomerName = ""
        _AppVersion = ""
    End Sub

    Public Property DeviceIMEI() As String
        Get
            DeviceIMEI = _DeviceIMEI
        End Get
        Set(ByVal value As String)
            _DeviceIMEI = value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            CustomerID = _CustomerID
        End Get
        Set(ByVal value As String)
            _CustomerID = value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            CustomerName = _CustomerName
        End Get
        Set(ByVal value As String)
            _CustomerName = value
        End Set
    End Property
    Public Property AppVersion() As String
        Get
            AppVersion = _AppVersion
        End Get
        Set(ByVal value As String)
            _AppVersion = value
        End Set
    End Property

End Class
