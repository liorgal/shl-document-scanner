﻿Option Explicit On
Option Strict On

Imports System.Xml.Serialization

<Serializable()> _
Public Class DocumentInfo

    Private _DeviceIMEI As String
    Private _CustomerID As String
    Private _DocRemarks As String
    Private _DocDestination As String
    Private _DocDestinationType As String
    Private _Document As String
    Private _DocType As String
    Private _Result As String
    Private _AppVersion As String

    Public Sub New()
        _DeviceIMEI = ""
        _CustomerID = ""
        _DocRemarks = ""
        _DocDestination = ""
        _DocDestinationType = ""
        _Document = ""
        _DocType = ""
        _Result = ""
        _AppVersion = ""
    End Sub

    Public Property DeviceIMEI() As String
        Get
            DeviceIMEI = _DeviceIMEI
        End Get
        Set(ByVal value As String)
            _DeviceIMEI = value
        End Set
    End Property
    Public Property CustomerID() As String
        Get
            CustomerID = _CustomerID
        End Get
        Set(ByVal value As String)
            _CustomerID = value
        End Set
    End Property
    Public Property DocRemarks() As String
        Get
            DocRemarks = _DocRemarks
        End Get
        Set(ByVal value As String)
            _DocRemarks = value
        End Set
    End Property

    Public Property DocDestination() As String
        Get
            DocDestination = _DocDestination
        End Get
        Set(ByVal value As String)
            _DocDestination = value
        End Set
    End Property
    Public Property DocDestinationType() As String
        Get
            DocDestinationType = _DocDestinationType
        End Get
        Set(ByVal value As String)
            _DocDestinationType = value
        End Set
    End Property
    Public Property Document() As String
        Get
            Document = _Document
        End Get
        Set(ByVal value As String)
            _Document = value
        End Set
    End Property
    Public Property DocType() As String
        Get
            DocType = _DocType
        End Get
        Set(ByVal value As String)
            _DocType = value
        End Set
    End Property
    Public Property Result() As String
        Get
            Result = _Result
        End Get
        Set(ByVal value As String)
            _Result = value
        End Set
    End Property
    Public Property AppVersion() As String
        Get
            AppVersion = _AppVersion
        End Get
        Set(ByVal value As String)
            _AppVersion = value
        End Set
    End Property


End Class
