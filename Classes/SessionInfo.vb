﻿Option Explicit On
Option Strict On

<Serializable()> _
Public Class SessionInfo

    Private _DeviceId As Integer
    Private _CallDateTime As DateTime
    Private _DeviceVersion As String
    Private _RemoteIP As String
    Private _RequestedService As String
    Private _TimeStamp As Long
    Private _AllRaw As String
    Private _QueryString As String


    Public Sub New()

        _DeviceId = 0
        _CallDateTime = Date.Now
        _DeviceVersion = ""
        _RemoteIP = ""
        _RequestedService = ""
        _TimeStamp = 0
        _AllRaw = ""
        _QueryString = ""

    End Sub

    Public Sub New(ByVal DeviceID As Integer, ByVal Context As System.Web.HttpContext)

        Me.New()

        Try
            If (String.IsNullOrEmpty(Context.Request.Headers("Version"))) Then
                _DeviceVersion = "unknown"
            Else
                _DeviceVersion = Context.Request.Headers("Version")
            End If

            _DeviceId = DeviceID
            _CallDateTime = Date.Now
            _RemoteIP = Context.IP '(.IP -> Added Extensions)
            _RequestedService = Context.Request.Url.ToString()
            _TimeStamp = Context.Timestamp.Ticks
            _AllRaw = Context.Request.ServerVariables("ALL_RAW")
            _QueryString = Context.Request.ServerVariables("QUERY_STRING")

        Catch Ex As Exception

        End Try

    End Sub

    Public Property DeviceId() As Integer
        Get
            DeviceId = _DeviceId
        End Get
        Set(ByVal value As Integer)
            _DeviceId = value
        End Set
    End Property
    Public Property CallDateTime() As DateTime
        Get
            CallDateTime = _CallDateTime
        End Get
        Set(ByVal value As DateTime)
            _CallDateTime = value
        End Set
    End Property
    Public Property DeviceVersion() As String
        Get
            DeviceVersion = _DeviceVersion
        End Get
        Set(ByVal value As String)
            _DeviceVersion = value
        End Set
    End Property
    Public Property RemoteIP() As String
        Get
            RemoteIP = _RemoteIP
        End Get
        Set(ByVal value As String)
            _RemoteIP = value
        End Set
    End Property
    Public Property RequestedService() As String
        Get
            RequestedService = _RequestedService
        End Get
        Set(ByVal value As String)
            _RequestedService = value
        End Set
    End Property
    Public Property TimeStamp() As Long
        Get
            TimeStamp = _TimeStamp
        End Get
        Set(ByVal value As Long)
            _TimeStamp = value
        End Set
    End Property
    Public Property AllRaw() As String
        Get
            AllRaw = _AllRaw
        End Get
        Set(ByVal value As String)
            _AllRaw = value
        End Set
    End Property
    Public Property QueryString() As String
        Get
            QueryString = _QueryString
        End Get
        Set(ByVal value As String)
            _QueryString = value
        End Set
    End Property

End Class
