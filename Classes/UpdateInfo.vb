﻿Option Explicit On
Option Strict On

<Serializable()> _
Public Class UpdateInfo

    Private _AppName As String
    Private _AppURL As String
    Private _CurrVersion As String
    Private _LatestVersion As String

    Public Sub New()

        _AppName = ""
        _AppURL = ""
        _CurrVersion = ""
        _LatestVersion = ""

    End Sub

    Public Property AppName() As String
        Get
            AppName = _AppName
        End Get
        Set(ByVal value As String)
            _AppName = value
        End Set
    End Property
    Public Property AppURL() As String
        Get
            AppURL = _AppURL
        End Get
        Set(ByVal value As String)
            _AppURL = value
        End Set
    End Property
    Public Property CurrVersion() As String
        Get
            CurrVersion = _CurrVersion
        End Get
        Set(ByVal value As String)
            _CurrVersion = value
        End Set
    End Property
    Public Property LatestVersion() As String
        Get
            LatestVersion = _LatestVersion
        End Get
        Set(ByVal value As String)
            _LatestVersion = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return "AppName: " & AppName & _
                " CurrVersion: " & CurrVersion & _
                " LatestVersion: " & LatestVersion & _
                " AppURL: " & AppURL
    End Function

End Class
