﻿Option Explicit On
Option Strict On

Imports System.Xml.Serialization

<Serializable()> _
<XmlRoot(ElementName:="Application")> _
Public Class Application

    Private _AppName As String
    Private _AppVersion As String
    Private _AppURL As String

    Public Sub New()
        _AppName = ""
        _AppVersion = ""
        _AppURL = ""
    End Sub

    Public Property AppName() As String
        Get
            AppName = _AppName
        End Get
        Set(ByVal value As String)
            _AppName = value
        End Set
    End Property
    Public Property AppVersion() As String
        Get
            AppVersion = _AppVersion
        End Get
        Set(ByVal value As String)
            _AppVersion = value
        End Set
    End Property
    Public Property AppURL() As String
        Get
            AppURL = _AppURL
        End Get
        Set(ByVal value As String)
            _AppURL = value
        End Set
    End Property

End Class
