﻿Option Explicit On
Option Strict On

Imports System.Xml.Serialization

<Serializable()> _
<XmlRoot(ElementName:="ApplicationUpdate")> _
Public Class ApplicationUpdate

    Private _Application As List(Of Application)

    Public Sub New()
        _Application = New List(Of Application)
    End Sub

    <XmlElement()> _
    Public ReadOnly Property Application() As List(Of Application)
        Get
            Return _Application
        End Get
    End Property

End Class
