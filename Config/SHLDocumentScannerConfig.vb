﻿Option Explicit On
Option Strict On

Imports System.Xml.Serialization

<Serializable()> _
<XmlRoot(ElementName:="SHLDocumentScannerConfig")> _
Public Class SHLDocumentScannerConfig

    Private _ApplicationUpdate As ApplicationUpdate

    Public Sub New()
        _ApplicationUpdate = New ApplicationUpdate
    End Sub

    Public Property ApplicationUpdate() As ApplicationUpdate
        Get
            ApplicationUpdate = _ApplicationUpdate
        End Get
        Set(ByVal value As ApplicationUpdate)
            _ApplicationUpdate = value
        End Set
    End Property

End Class
