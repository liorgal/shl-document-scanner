﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Xml.Serialization
Imports System.ComponentModel
Imports System.Reflection

Public Module SHLDocumentScannerConfigManager

    Public mySHLDocumentScannerConfig As SHLDocumentScannerConfig

    Private Const SHL_DOCUMENT_SCANNER_CONFIG_FILE_NAME As String = "Config\SHLDocumentScannerConfig.xml"

    Public Function ReadExternalConfig(Optional ByVal strFileName As String = "") As SHLDocumentScannerConfig

        Dim myConfig As New SHLDocumentScannerConfig

        If strFileName = "" Then
            strFileName = GetExternalConfigPath()
        End If

        Call DebugOutput("Read External Config : " & strFileName, DebugMode.Maximal)

        Dim myReader As New StreamReader(strFileName)

        Try

            Dim x As New XmlSerializer(myConfig.GetType)
            myConfig = CType(x.Deserialize(myReader), SHLDocumentScannerConfig)
            myReader.Close()
            ReadExternalConfig = myConfig
            x = Nothing

        Catch Ex As Exception
            Call DebugOutput("Error: " & Ex.ToString, DebugMode.Normal)
            Call DebugOutput("Error: Requested Config File: " & strFileName, DebugMode.Normal)
            Return Nothing

        Finally
            myConfig = Nothing
            myReader.Dispose()
            myReader = Nothing
        End Try

    End Function

    Public Function WriteExternalConfig(ByVal currSHLDocumentScannerConfig As SHLDocumentScannerConfig, Optional ByVal strFileName As String = "") As Boolean

        Try

            If strFileName = "" Then
                strFileName = GetExternalConfigPath()
            End If

            Dim myWriter As New StreamWriter(strFileName)
            Dim ExtraTypes(0) As System.Type

            Dim a(0) As String
            ExtraTypes(0) = a.GetType

            Dim myXmlSerializer As New XmlSerializer(currSHLDocumentScannerConfig.GetType, ExtraTypes)
            myXmlSerializer.Serialize(myWriter, currSHLDocumentScannerConfig)

            myXmlSerializer = Nothing

            myWriter.Close()
            myWriter = Nothing

            Return True

        Catch Ex As Exception
            Return False
            'TODO: Add Code Here...
        End Try

    End Function

    Private Function GetExternalConfigPath() As String

        Try
            Dim strFileName As String = IIf(HttpContext.Current.Server.MapPath("~").EndsWith("\"), _
                                          HttpContext.Current.Server.MapPath("~"), _
                                          HttpContext.Current.Server.MapPath("~") & "\").ToString _
                                          & SHL_DOCUMENT_SCANNER_CONFIG_FILE_NAME
            Return strFileName

        Catch Ex As Exception
            Return ""
        End Try

    End Function

End Module
