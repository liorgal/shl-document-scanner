﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(namespace:="", name:="SHL Document Scanner", Description:="The SHL Document Scanner Webservice")> _
<ToolboxItem(False)> _
Public Class DocScanService
    Inherits System.Web.Services.WebService

    '**************************************************************************
    'Maximum request length exceeded
    'http://stackoverflow.com/questions/3853767/maximum-request-length-exceeded
    '**************************************************************************

    Public Sub New()

        Try
            CountryCode = CInt(My.Settings.CountryCode)
        Catch Ex As Exception
            CountryCode = 49
        End Try

        Try
            MCDBlibConfig = My.Settings.MCDBlibConfig
        Catch Ex As Exception
            MCDBlibConfig = "C:\MC\CCM_MCDBlib_Config.xml"
        End Try

        Dim bRetVal = SetDebugSessionId(GetSessionID())

        Call DebugOutput("", DebugMode.Normal)

        Call DebugOutput("ShlDocSan Service Started!", DebugMode.Normal)
        Call DebugOutput("MC Config File  : " & MCDBlibConfig, DebugMode.Normal)
        Call DebugOutput("Country Code    : " & CountryCode.ToString, DebugMode.Normal)
        Call DebugOutput("ShlDocSan.dll   : " & My.Application.Info.Version.ToString & " (" & GetModifiedFileDate(My.Application.Info.DirectoryPath & "\" & My.Application.Info.ProductName & ".dll") & ")", DebugMode.Normal)


        Call mcDBlib.Set_Config(MCDBlibConfig)
        mcDBlib.sWorkStation = Left(My.User.Name & "@" & My.Computer.Name, 50).Trim
        '--- OR ---
        ''Getting ASP.NET IIS User Name (http://richhewlett.com/2011/02/15/getting-a-users-username-in-asp-net/)
        'mcDBlib.sWorkStation = Left(HttpContext.Current.Request.LogonUserIdentity.Name, 50).Trim

        'Read External CCMService Config
        'myCCMServiceConfig = ReadExternalConfig()

    End Sub

    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function GetCustomerInfo(ByVal XMLData As String) As XmlDocument
        Dim myGetCustomerMethod As New CustomerInfoMethod(Context, XMLData)
        GetCustomerInfo = myGetCustomerMethod.GetCustomer
        myGetCustomerMethod = Nothing
    End Function

    <WebMethod()> _
    Public Function SetDocument(ByVal XMLData As String) As XmlDocument

        '********************************************************************************************
        'If you are using IIS for hosting your application, then the default upload file size if 4MB. 
        'To increase it, please use this below section in your web.config -
        '<configuration>
        '    <system.web>
        '        <httpRuntime maxRequestLength="1048576" />
        '    </system.web>
        '</configuration>
        '
        'For IIS7 and above, you also need to add the lines below:
        ' <system.webServer>
        '   <security>
        '      <requestFiltering>
        '         <requestLimits maxAllowedContentLength="1073741824" />
        '      </requestFiltering>
        '   </security>
        ' </system.webServer>
        '********************************************************************************************


        Dim mySetDocumentMethod As New SetDocumentMethod(Context, XMLData)
        SetDocument = mySetDocumentMethod.SetDocument
        mySetDocumentMethod = Nothing
    End Function

    <WebMethod()> _
    Public Function GetApplicationUpdate(ByVal XMLData As String) As XmlDocument
        Dim myGetApplicationUpdateMethod As New GetApplicationUpdateMethod(Context, XMLData)
        GetApplicationUpdate = myGetApplicationUpdateMethod.GetApplicationUpdate
        myGetApplicationUpdateMethod = Nothing
    End Function

    Private Function GetSessionID() As String

        Try
            '************************************************************
            '*           Store Timestamp.Ticks as SessionID             *
            '************************************************************
            'To use it: 
            '   Add [enableSession:=True] to WebMethod Declaraion
            '************************************************************
            'DebugSessionId = "[" & Context.Timestamp.Ticks.ToString & "]"


            '************************************************************
            '*      Store Last 8 Characters from Session.SessionID      *
            '*                      as SessionID                        *
            '************************************************************
            'To use it: 
            '   Add [enableSession:=True] to WebMethod Declaraion
            '************************************************************
            'DebugSessionId = "[" & Right(Context.Session.SessionID.ToUpper, 8) & "]"


            '************************************************************
            '*      Store Last 8 Characters from GUID as SessionID      *
            '************************************************************
            'Return "[" & Right(Guid.NewGuid.ToString.ToUpper, 8) & "]"


            '************************************************************
            '*          Store REMOTE_ADDR as SessionID                  *
            '************************************************************
            'Return "[" & Context.Request.ServerVariables("REMOTE_ADDR").Trim & "]"


            '************************************************************
            '*      Store Smartphone Device IP Address as SessionID     *
            '************************************************************
            Return "[" & Context.IP & "]"

        Catch Ex As Exception
            Return "[UNKNOWN]"
        End Try

    End Function

    Private Sub ShlDocSan_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        GC.Collect()
    End Sub

End Class