﻿Option Explicit On
Option Strict On

Imports System.Xml
Imports mcDBlib


Public Class CustomerInfoMethod

    Private _HttpContext As HttpContext
    Private _XmlData As String
    Private _DeviceIMEI As String
    Private _SessionInfo As SessionInfo

    Const METHOD_MANE As String = "CustomerInfo"

    Public Sub New(ByVal Context As HttpContext, ByVal XmlData As String)
        Call DebugOutput("***************** " & METHOD_MANE & " *****************", DebugMode.Normal)
        Call DebugOutput("XML Data is: " & IIf(XmlData.Trim = "", "", vbNewLine & XmlData).ToString, DebugMode.Normal)

        _HttpContext = Context
        _XmlData = NormalizeXmlString(XmlData)

        _SessionInfo = New SessionInfo(0, _HttpContext)

    End Sub

    Public Function GetCustomer() As XmlDocument

        Dim XmlResults As XmlDocument = Nothing
        Dim _ErrorCode As Byte

        Try

            XmlResults = ProcessData()

        Catch Ex As Exception
            Call DebugOutput("Exception: " & Ex.Message, DebugMode.Normal)

            _ErrorCode = GetErrorCode(Ex)

            If _ErrorCode = ErrorCode.UnknownError Then
                Call DebugOutput("StackTrace: " & vbNewLine & Ex.StackTrace, DebugMode.Normal)
                If Ex.InnerException IsNot Nothing Then
                    Call DebugOutput("Inner Exception : " & vbNewLine & Ex.InnerException.Message, DebugMode.Normal)
                    Call DebugOutput("Inner StackTrace : " & vbNewLine & Ex.InnerException.StackTrace, DebugMode.Normal)
                End If
            End If

        Finally

            If XmlResults IsNot Nothing Then
                Call DebugOutput("XML Output is: " & vbNewLine & XmlResults.OuterXml, DebugMode.Normal)
            Else
                Call DebugOutput("No output.", DebugMode.Normal)
            End If

            Call DebugOutput("ResponseTime: " & GetResponseTime(_SessionInfo).ToString, DebugMode.Normal)

            Call DebugOutput(METHOD_MANE & " Ended With Code: " & _ErrorCode.ToString, DebugMode.Normal)

            'Write to LogFile
            Call LogOutput("Device IMEI: " & _DeviceIMEI.PadRight(15) & " Method: " & METHOD_MANE.PadRight(30) & " Call Result: " & _ErrorCode.ToString.PadRight(3), LogMode.Normal)

        End Try

        Return XmlResults

    End Function

    Private Function ProcessData() As XmlDocument

        Dim myCustomerInfo As New CustomerInfo
        Try
            Dim swValidDevice As Boolean = False
            Dim intTechStatus As Integer = 0

#If DEBUG Then
           'For Debug Only!!!
            _XmlData = <CustomerInfo>
                           <AppVersion>1.6</AppVersion>
                           <CustomerID>47411319</CustomerID>
                           <CustomerName></CustomerName>
                           <DeviceIMEI>359544057594396</DeviceIMEI>
                       </CustomerInfo>.ToString
#End If

            'Load XML String
            Dim XmlRoot As XElement = XElement.Parse(_XmlData)

            If XmlRoot.Name <> "CustomerInfo" Then
                Return ToXmlDocument(Of CustomerInfo)(myCustomerInfo)
            End If

            'Dim XmlTag As XElement = XElement.Parse(_XmlData).Element("CustomerInfo")

            'If XmlTag Is Nothing Then
            '    'Wrong XML Detected
            '    Return ToXmlDocument(Of CustomerInfo)(myCustomerInfo)
            'End If

            myCustomerInfo = DeserializeXmlData(Of CustomerInfo)(XmlRoot.ToString)

            'Remove Leading Zero from CustomerID
            myCustomerInfo.CustomerID = myCustomerInfo.CustomerID.Trim.TrimStart(CChar("0"))

            'Store DeviceIMEI
            _DeviceIMEI = myCustomerInfo.DeviceIMEI.Trim

            Dim myTECHNICIANS As New clsTECHNICIANS
            Call myTECHNICIANS.SearchByIMEI(myCustomerInfo.DeviceIMEI)
            If myTECHNICIANS.IsMainTable_Populated Then
                For Each DR As DataRow In myTECHNICIANS.MainTable.Rows
                    If DR("TECHIMEI").ToString.Trim = myCustomerInfo.DeviceIMEI Then
                        'Store Tech Status
                        intTechStatus = CInt(DR("TECHSTATUS"))
                        swValidDevice = True
                        Exit For
                    End If

                Next
            Else
                'Smartphone ID not found
                Call DebugOutput("Smartphone ID (" & myCustomerInfo.DeviceIMEI & ") not found!", DebugMode.Normal)
                myCustomerInfo.CustomerID = ""
                myCustomerInfo.DeviceIMEI = ""
                swValidDevice = False
            End If
            myTECHNICIANS = Nothing

            If swValidDevice = True Then

                Select Case intTechStatus
                    Case TechStatus.ShlTechnician, TechStatus.ShlSalesman, TechStatus.ShlMedicalStaff, TechStatus.ShlMicuStaff
                        Call DebugOutput("Internal Employee Detected!", DebugMode.Normal)
                        'Search Patient
                        Dim myCustomerData As New clsCustomerData
                        Call myCustomerData.Search_byCustID(CLng(myCustomerInfo.CustomerID))
                        If myCustomerData.IsMainTable_Populated Then
                            Dim Cust = myCustomerData.MainTable.Rows(0)
                            myCustomerInfo.CustomerName = Cust("FULLNAME").ToString.Trim
                            myCustomerInfo.CustomerID = Cust("CUSTID").ToString.Trim
                        End If
                        myCustomerData = Nothing

                    Case TechStatus.BiotronikEmployee, TechStatus.TigburEmployee, TechStatus.TzabarRefuaEmployee
                        Call DebugOutput("External Employee Detected!", DebugMode.Normal)
                        'Return Empty Patient
                        myCustomerInfo.CustomerName = ""
                        myCustomerInfo.CustomerID = ""

                    Case TechStatus.AccountDisable
                        Call DebugOutput("Account Disable Employee Detected!", DebugMode.Normal)
                        myCustomerInfo.CustomerID = ""
                        myCustomerInfo.DeviceIMEI = ""

                    Case Else
                        Call DebugOutput("Unknown/Undefined Tech Status Detected! (" & intTechStatus.ToString & ")", DebugMode.Normal)
                        myCustomerInfo.CustomerID = ""
                        myCustomerInfo.DeviceIMEI = ""

                End Select

                'Return TechStatus field as am AppVersion to the Apllication
                myCustomerInfo.AppVersion = intTechStatus.ToString

            End If

            Return ToXmlDocument(Of CustomerInfo)(myCustomerInfo)

        Catch Ex As Exception
            If GetErrorCode(Ex) = ErrorCode.UnknownError Then
                'Set General WS Error Message
                Throw New WSException(Ex.Message, Ex)
            Else
                Throw Ex
            End If

        Finally
            myCustomerInfo = Nothing

        End Try

    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _HttpContext = Nothing
        _SessionInfo = Nothing
    End Sub

End Class
