﻿Option Explicit On
Option Strict On

Imports System.Xml
Imports System.Reflection.MethodBase

Public Class GetApplicationUpdateMethod

    Private _HttpContext As HttpContext
    Private _XmlData As String
    Private _SessionInfo As SessionInfo

    Const METHOD_MANE As String = "GetApplicationUpdate"

    Public Sub New(ByVal Context As HttpContext, ByVal XmlData As String)
        Call DebugOutput("***************** " & METHOD_MANE & " *****************", DebugMode.Normal)
        Call DebugOutput("XML Data is: " & IIf(XmlData.Trim = "", "", vbNewLine & XmlData).ToString, DebugMode.Normal)

        _HttpContext = Context
        _XmlData = NormalizeXmlString(XmlData)

        _SessionInfo = New SessionInfo(0, _HttpContext)

    End Sub

    Public Function GetApplicationUpdate() As XmlDocument

        Dim XmlResults As XmlDocument = Nothing
        Dim _ErrorCode As Byte

        Try

            XmlResults = ProcessData()

        Catch Ex As Exception
            Call DebugOutput("Exception: " & Ex.Message, DebugMode.Normal)

            _ErrorCode = GetErrorCode(Ex)

            If _ErrorCode = ErrorCode.UnknownError Then
                Call DebugOutput("StackTrace: " & vbNewLine & Ex.StackTrace, DebugMode.Normal)
                If Ex.InnerException IsNot Nothing Then
                    Call DebugOutput("Inner Exception : " & vbNewLine & Ex.InnerException.Message, DebugMode.Normal)
                    Call DebugOutput("Inner StackTrace : " & vbNewLine & Ex.InnerException.StackTrace, DebugMode.Normal)
                End If
            End If

            'XmlResults = ToXmlDocument(Of Ack)(New Ack(_ErrorCode.ToString))

        Finally

            If XmlResults IsNot Nothing Then
                Call DebugOutput("XML Output is: " & vbNewLine & XmlResults.OuterXml, DebugMode.Normal)
            Else
                Call DebugOutput("No output.", DebugMode.Normal)
            End If

            Call DebugOutput("ResponseTime: " & GetResponseTime(_SessionInfo).ToString, DebugMode.Normal)

            Call DebugOutput(METHOD_MANE & " Ended With Code: " & _ErrorCode.ToString, DebugMode.Normal)

        End Try

        Return XmlResults

    End Function

    Private Function ProcessData() As XmlDocument

        Dim myUpdateInfo As New UpdateInfo
        Try
            Dim swValidDevice As Boolean = False

            'For Debug Only!!!
            '_XmlData = <UpdateInfo>
            '               <AppName>com.shl.documentscanner</AppName>
            '               <AppURL></AppURL>
            '               <CurrVersion>1.1</CurrVersion>
            '               <LatestVersion></LatestVersion>
            '           </UpdateInfo>.ToString()

            'Load XML String
            Dim XmlRoot As XElement = XElement.Parse(_XmlData)

            If XmlRoot.Name <> "UpdateInfo" Then
                Return ToXmlDocument(Of UpdateInfo)(myUpdateInfo)
            End If

            myUpdateInfo = DeserializeXmlData(Of UpdateInfo)(XmlRoot.ToString)

            Call DebugOutput(myUpdateInfo.ToString, DebugMode.Normal)

            Dim myConfig As SHLDocumentScannerConfig

            'Read External MICUCommunicator Config
            myConfig = ReadExternalConfig()

            For I As Integer = 0 To myConfig.ApplicationUpdate.Application.Count - 1
                If myConfig.ApplicationUpdate.Application(I).AppName.ToLower.Trim = myUpdateInfo.AppName.ToLower.Trim Then
                    myUpdateInfo.LatestVersion = myConfig.ApplicationUpdate.Application(I).AppVersion
                    myUpdateInfo.AppURL = myConfig.ApplicationUpdate.Application(I).AppURL
                    Exit For
                End If
            Next

            myConfig = Nothing

            Return ToXmlDocument(Of UpdateInfo)(myUpdateInfo)

        Catch Ex As Exception
            If GetErrorCode(Ex) = ErrorCode.UnknownError Then
                'Set General WS Error Message
                Throw New WSException(Ex.Message, Ex)
            Else
                Throw Ex
            End If

        Finally
            myUpdateInfo = Nothing

        End Try

    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _HttpContext = Nothing
        _SessionInfo = Nothing
    End Sub

End Class
