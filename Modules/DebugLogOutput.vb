﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Threading
Imports System.Runtime.Remoting.Messaging
Imports System.Security.AccessControl
Imports System.Security.Principal

Module DebugLogOutput

    'Public DebugSessionId As String = ""
    <ThreadStatic()> _
    Private _DebugSessionId As String = ""

    Private Const DEBUG_MUTEX_NAME As String = "ShlDocumentScanner"

    Public Enum LogMode As Integer
        Minimal = 0
        Normal = 1
        Maximal = 2
    End Enum

    Public Enum DebugMode As Integer
        Minimal = 0
        Normal = 1
        Maximal = 2
    End Enum

    'Debug/Log Output Functions
    Public Sub LogOutput(ByVal Message As String, ByVal LogMode As LogMode)

        If My.Settings.LogMode = "0" Then Exit Sub

        If LogMode > CInt(My.Settings.LogMode) Then Exit Sub

        'Dim myMutex As New MutexLocker(DEBUG_MUTEX_NAME)
        'If myMutex.IsLocked Then

        'Swith To Admin User
        Call ImpersonateAdmin()

        Call WriteToLogFile(Message)

        'Swith Back To Original User
        Call UndoImpersonation()

        'End If
        'myMutex.Dispose()
        'myMutex = Nothing

    End Sub

    Public Sub DebugOutput(ByVal Message As String, ByVal DebugMode As DebugMode)

        If My.Settings.DebugMode = "0" Then Exit Sub

        If DebugMode > CInt(My.Settings.DebugMode) Then Exit Sub

        Dim myMutex As New MutexLocker(DEBUG_MUTEX_NAME)
        Try
            If myMutex.IsLocked Then
                'Swith To Admin User
                Call ImpersonateAdmin()

                Call WriteToDebugFile(Message)

                'Swith Back To Original User
                Call UndoImpersonation()
            End If
        Catch Ex As Exception
            Call Debug.Print(Ex.Message)
        Finally
            myMutex.Dispose()
            myMutex = Nothing
        End Try

    End Sub

    Public Sub AlertOutput(ByVal Message As String)

        'Swith To Admin User
        Call ImpersonateAdmin()

        Dim bRetVal As Boolean = WriteToEventLog(Message)

        'Swith Back To Original User
        Call UndoImpersonation()

    End Sub

    Private Sub WriteToLogFile(ByVal strMessage As String)

        Dim FilePath As String
        Dim ArchFilePath As String
        Dim LogFolderPath As String
        Const LOG_FOLDER_NAME As String = "LOG"

        If My.Settings.LogMode = "0" Then Exit Sub

        If My.Settings.LogPath.Trim = "" Then
            LogFolderPath = My.Application.Info.DirectoryPath & "\" & LOG_FOLDER_NAME
        Else
            LogFolderPath = My.Settings.LogPath.Trim()
        End If

        'Check for "\" at the end of the path
        If Not LogFolderPath.EndsWith("\") Then
            LogFolderPath = LogFolderPath & "\"
        End If

        'Check If Log folder Exist
        If Not My.Computer.FileSystem.DirectoryExists(LogFolderPath) Then
            My.Computer.FileSystem.CreateDirectory(LogFolderPath)
        End If

        'Set Log File path
        FilePath = LogFolderPath & My.Application.Info.ProductName & ".LOG"

        'Change name of the Log file every day at night.
        If File.Exists(FilePath) Then
            If DateDiff("d", Format(File.GetCreationTime(FilePath), "Short Date"), Format(Date.Now, "Short Date")) >= 1 Then
                'Set Archive File name
                ArchFilePath = LogFolderPath & My.Application.Info.ProductName & "-" & DateAdd(DateInterval.Day, -1, Date.Now).ToString("yyyyMMdd") & ".LOG"
                Try
                    File.Move(FilePath, ArchFilePath)
                Catch Ex As Exception
                    Debug.Print(Ex.Message)
                End Try
            End If
        End If

        'Write LOG using StreamWiter
        Dim objWriter As StreamWriter
        Try
            objWriter = New StreamWriter(FilePath, Append:=True)
            objWriter.Write(Date.Now & " : " & strMessage & vbNewLine)
            objWriter.Close()
        Catch Ex As Exception
            'MsgBox(Ex.Message())
        Finally
            objWriter = Nothing
        End Try

    End Sub

    Private Sub WriteToDebugFile(ByVal strMessage As String)

        Try
            Dim FilePath As String
            Dim ArchFilePath As String
            Dim LogFolderPath As String
            Const LOG_FOLDER_NAME As String = "LOG"
            Const MAX_DEBUG_FILE_SIZE As Integer = 10485760 '10 Mb (value in byte = 10 * 1024 * 1024 (sise * kilobyte * byte))

            Dim swNewFileCreated As Boolean = False

            If My.Settings.DebugMode = "0" Then Exit Sub

            If My.Settings.LogPath.Trim = "" Then
                LogFolderPath = My.Application.Info.DirectoryPath & "\" & LOG_FOLDER_NAME
            Else
                LogFolderPath = My.Settings.LogPath.Trim()
            End If

            'Check for "\" at the end of the path
            If Not LogFolderPath.EndsWith("\") Then
                LogFolderPath = LogFolderPath & "\"
            End If

            'Check If Log folder Exist
            If Not My.Computer.FileSystem.DirectoryExists(LogFolderPath) Then
                My.Computer.FileSystem.CreateDirectory(LogFolderPath)
            End If

            'Set Log File path
            FilePath = LogFolderPath & My.Application.Info.ProductName & ".DBG"

            'Change name of the Debug file every day at night.
            If File.Exists(FilePath) Then
                If DateDiff("d", Format(File.GetCreationTime(FilePath), "Short Date"), Format(Date.Now, "Short Date")) >= 1 Then
                    'Set Archive File name
                    ArchFilePath = LogFolderPath & My.Application.Info.ProductName & "-" & DateAdd(DateInterval.Day, -1, Date.Now).ToString("yyyyMMdd") & ".DBG"

                    'Delete Previous File (If Exists)
                    Try
                        If File.Exists(ArchFilePath) Then
                            File.Delete(ArchFilePath)
                        End If
                    Catch Ex As Exception
                        Debug.Print(Ex.Message)
                    End Try

                    'Rename Debug File 
                    Try
                        File.Move(FilePath, ArchFilePath)
                        swNewFileCreated = True
                    Catch Ex As Exception
                        Debug.Print(Ex.Message)
                    End Try
                End If
            End If

            'Check Debug File Size
            If File.Exists(FilePath) Then
                Dim myFileInfo As New FileInfo(FilePath)
                If myFileInfo.Length >= MAX_DEBUG_FILE_SIZE Then
                    For I As Integer = 0 To 999
                        'Set Archive File Name
                        ArchFilePath = LogFolderPath & My.Application.Info.ProductName & "-" & Format(Date.Now, "yyyyMMdd") & "-Part" & I.ToString.PadLeft(3, CChar("0")) & ".DBG"
                        If Not File.Exists(ArchFilePath) Then
                            Try
                                File.Move(FilePath, ArchFilePath)
                                swNewFileCreated = True
                            Catch Ex As Exception
                                Debug.Print(Ex.Message)
                            End Try
                            Exit For
                        End If
                    Next
                End If
                myFileInfo = Nothing
            End If

            ''Write Debug using TextWriter
            'Dim objWriter As TextWriter
            'Try
            '    'objWriter = TextWriter.Synchronized(File.AppendText(FilePath))
            '    objWriter = New StreamWriter(FilePath, Append:=True)
            '    If DebugSessionId.Trim <> "" Then
            '        objWriter.Write(Date.Now & " : " & DebugSessionId.Trim & " : " & strMessage & vbNewLine)
            '    Else
            '        objWriter.Write(Date.Now & " : " & strMessage & vbNewLine)
            '    End If
            '    objWriter.Flush()
            '    objWriter.Close()
            'Catch Ex As Exception
            '    'MsgBox(Ex.Message())
            'Finally
            '    objWriter = Nothing
            'End Try

            'Write Debug using StreamWriter
            Try
                Dim strToWrite As String = ""
                Using objWriter As StreamWriter = File.AppendText(FilePath)
                    strToWrite = strToWrite & Date.Now & " : "
                    If Not IsNothing(GetDebugSessionId()) Then
                        If GetDebugSessionId().Trim <> "" Then
                            strToWrite = strToWrite & GetDebugSessionId() & " : "
                        End If
                    End If
                    strToWrite = strToWrite & strMessage & vbNewLine
                    objWriter.Write(strToWrite)
                    objWriter.Flush()
                    objWriter.Close()
                End Using
            Catch Ex As Exception
                Debug.Print(Ex.Message)
            End Try

            ''Write Debug using StreamWriter & Named Mutex
            ''(If a Mutex is named, it is eligible to be a system-wide Mutex that can be accessed from multiple processes)
            'Dim mMtx As Mutex = New Mutex(False, "Global\" & DEBUG_MUTEX_NAME)
            'If mMtx.WaitOne = True Then
            '    Try
            '        Using objWriter As StreamWriter = File.AppendText(FilePath)
            '            If GetDebugSessionId().Trim <> "" Then
            '                'objWriter.Write(Date.Now & " : " & DebugSessionId.Trim & " : " & strMessage & vbNewLine)
            '                objWriter.Write(Date.Now & " : " & GetDebugSessionId() & " : " & strMessage & vbNewLine)
            '            Else
            '                objWriter.Write(Date.Now & " : " & strMessage & vbNewLine)
            '            End If

            '            objWriter.Flush()
            '            objWriter.Close()
            '        End Using

            '    Catch Ex As AbandonedMutexException
            '        'MsgBox(Ex.Message())
            '    Catch Ex As Exception
            '        'MsgBox(Ex.Message())
            '    Finally
            '        mMtx.ReleaseMutex()
            '    End Try
            'End If
            'If Not mMtx Is Nothing Then mMtx.Close()
            'mMtx = Nothing

            '***************
            'WORKED SOLUTION
            '***************
            'Dim intRetriesCount As Integer = 0
            'Const MAX_RETRIES_COUNT As Integer = 10

            ''Write Debug using StreamWiter
            'Do While True
            '    If Not IsFileUsedByAnotherProcess(FilePath) Then
            '        Dim objWriter As StreamWriter
            '        Try
            '            'objWriter = New StreamWriter(FilePath, Append:=True)
            '            objWriter = New StreamWriter(File.Open(FilePath, FileMode.Append, FileAccess.Write, FileShare.None))
            '            If DebugSessionId.Trim <> "" Then
            '                objWriter.Write(Date.Now & " : " & DebugSessionId.Trim & " : " & strMessage & vbNewLine)
            '            Else
            '                objWriter.Write(Date.Now & " : " & strMessage & vbNewLine)
            '            End If
            '            objWriter.Flush()
            '            objWriter.Close()
            '        Catch Ex As Exception
            '            'MsgBox(Ex.Message())
            '        Finally
            '            objWriter = Nothing
            '        End Try
            '        Exit Do
            '    Else
            '        intRetriesCount += 1
            '        If intRetriesCount > MAX_RETRIES_COUNT Then
            '            Exit Do
            '        End If
            '    End If
            'Loop

            'Update File Creation Time
            If swNewFileCreated Then
                File.SetCreationTime(FilePath, Date.Now)
            End If

        Catch Ex As Exception
            Debug.Print(Ex.Message)
        End Try

    End Sub

    Private Function IsFileUsedByAnotherProcess(ByVal strFileName As String) As Boolean

        Try
            'If Not File.Exists(strFileName) Then
            '    Return False
            'End If
            Using fs As FileStream = File.Open(strFileName, FileMode.Append, FileAccess.Write, FileShare.None)
            End Using
            Return False
        Catch Ex As IOException
            Call Debug.Print(Now() & "  " & strFileName & " file Locked!")
            Return True
        End Try

    End Function

    'Debug SessionID
    Public Function SetDebugSessionId(ByVal strValue As String) As Boolean

        Try
            'Using Session State (Todo use this add [enableSession:=True] to WebMethod declaraion) 
            'HttpContext.Current.Session("DebugSessionId") = strValue

            'Using CallContext
            'CallContext.SetData("DebugSessionId", strValue)

            'Using ThreadStatic
            _DebugSessionId = strValue

            Return True

        Catch Ex As Exception
            Return False
        End Try

    End Function

    Public Function GetDebugSessionId() As String

        Try
            'Using Session State (Todo use this add [enableSession:=True] to WebMethod declaraion) 
            'Return HttpContext.Current.Session("DebugSessionId").ToString

            'Using CallContext
            'Return CallContext.GetData("DebugSessionId").ToString

            'Using ThreadStatic
            Return _DebugSessionId

        Catch Ex As Exception
            Return ""
        End Try

    End Function

    'Private class for Mutex Implementation
    Private Class MutexLocker

        Implements IDisposable
        Private _Mutex As Mutex

        'Public Sub New(ByVal mutexId As String)

        '    Dim swCreatedNew As Boolean

        '    Dim mtxSecurityIdentifier As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

        '    Dim mtxAccessRule As New MutexAccessRule(mtxSecurityIdentifier, _
        '                                             MutexRights.Synchronize Or MutexRights.Modify, _
        '                                             AccessControlType.Allow)
        '    Dim mtxSecurity As New MutexSecurity()

        '    mtxSecurity.AddAccessRule(mtxAccessRule)

        '    'mtxSecurity.AddAccessRule(New MutexAccessRule( _
        '    '                                            New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing), _
        '    '                                            MutexRights.Synchronize Or MutexRights.Modify, _
        '    '                                            AccessControlType.Allow))

        '    Try
        '        'attempt to create the mutex, with the desired DACL..
        '        _Mutex = New Mutex(False, mutexId, swCreatedNew, mtxSecurity)

        '    Catch Ex As AbandonedMutexException
        '        Throw Ex

        '    Catch Ex As WaitHandleCannotBeOpenedException
        '        'the mutex cannot be opened, probably because a Win32 object of a different
        '        'type with the same name already exists.
        '        Throw Ex

        '    Catch Ex As UnauthorizedAccessException
        '        'the mutex exists, but the current process or thread token does not
        '        'have permission to open the mutex with SYNCHRONIZE | MUTEX_MODIFY rights.
        '        Throw Ex

        '    End Try

        'End Sub

        Public Sub New(ByVal mutexId As String)

            Try
                'attempt to create the mutex
                _Mutex = New Mutex(False, mutexId)

            Catch Ex As AbandonedMutexException
                Throw Ex

            Catch Ex As WaitHandleCannotBeOpenedException
                'the mutex cannot be opened, probably because a Win32 object of a different
                'type with the same name already exists.
                Throw Ex

            Catch Ex As UnauthorizedAccessException
                'the mutex exists, but the current process or thread token does not
                'have permission to open the mutex with SYNCHRONIZE | MUTEX_MODIFY rights.
                Throw Ex

            End Try

        End Sub

        Public Function IsLocked() As Boolean

            Try
                If _Mutex.WaitOne = True Then
                    Return True
                Else
                    Return False
                End If

            Catch Ex As Exception
                Throw
            End Try

        End Function

        Public Sub Dispose() Implements IDisposable.Dispose

            If _Mutex IsNot Nothing Then
                _Mutex.ReleaseMutex()
                _Mutex.Close()
            End If

            _Mutex = Nothing

        End Sub

    End Class

End Module

'*******************************************************
'Error Received on Using Mutes inside Impersonalisation:
'*******************************************************
'System.IO.IOException: Either a required impersonation level was not provided, or the provided impersonation level is invalid.

'   at System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)
'   at System.Threading.Mutex.<>c__DisplayClass3.<.ctor>b__0(Object userData)
'   at System.Runtime.CompilerServices.RuntimeHelpers.ExecuteCodeWithGuaranteedCleanup(TryCode code, CleanupCode backoutCode, Object userData)
'   at System.Threading.Mutex..ctor(Boolean initiallyOwned, String name, Boolean& createdNew, MutexSecurity mutexSecurity)
'   at SHL_Document_Scanner.DebugLogOutput.WriteToDebugFile(String strMessage)
'   at SHL_Document_Scanner.DebugLogOutput.DebugOutput(String Message, DebugMode DebugMode)
'   at SHL_Document_Scanner.DocScanService..ctor()




