﻿Option Explicit On
Option Strict On

Imports System.Security

Module EventLogOutput

    Private Const CURRENT_APPLICATION_NAME As String = "SHL Document Scanner Alerts"

    Public Function WriteToEventLog(ByVal strEntry As String, _
                                    Optional ByVal strSource As String = CURRENT_APPLICATION_NAME, _
                                    Optional ByVal strAppName As String = CURRENT_APPLICATION_NAME, _
                                    Optional ByVal evtEventType As EventLogEntryType = EventLogEntryType.Information, _
                                    Optional ByVal strEventLogName As String = CURRENT_APPLICATION_NAME) As Boolean
        '**********************************************************************************************
        'PURPOSE: Write Entry to Event Log using VB.NET
        'PARAMETERS: Entry - Value to Write
        '            AppName - Name of Client Application. Needed 
        '                       because before writing to event log, you must 
        '                       have a named EventLog source. 
        '            EventType - Entry Type, from EventLogEntryType 
        '                       Structure e.g., EventLogEntryType.Warning, 
        '                       EventLogEntryType.Error
        '            LogName: Name of Log (System, Application; 
        '                       Security is read-only) If you 
        '                       specify a non-existent log, the log will be
        '                       created

        'RETURNS:   True if successful, false if not
        '***********************************************************************************************
        Try
            'Check/Create EventLog
            Dim RetVal As Boolean = CreateEventLog(strEventLogName)
            Dim ev As New EventLog(logname:=CURRENT_APPLICATION_NAME, _
                                    machinename:=System.Environment.MachineName, _
                                    Source:=strSource)
            ev.WriteEntry(message:=strEntry, type:=evtEventType, eventID:=CInt("1000"))
            ev.Close()
        Catch secEx As SecurityException
            'Error writing to the event log: this may be due to a lack of appropriate permissions
            'secEx.Message()
        Catch ex As Exception
            'Error accessing logs on the local machine
            'ex.Message()
        End Try

    End Function

    Private Function CreateEventLog(ByVal EventLogName As String) As Boolean
        Dim ev As New EventLog
        Try
            'Check for the existence of the log 
            If Not EventLog.Exists(EventLogName) Then
                ' If the event source is already registered we want to delete it 
                ' before recreating it on the call to CreateEventSource
                If EventLog.SourceExists(CURRENT_APPLICATION_NAME & " EventLog") Then
                    EventLog.DeleteEventSource(CURRENT_APPLICATION_NAME & " EventLog")
                End If
                EventLog.CreateEventSource(CURRENT_APPLICATION_NAME & " EventLog", EventLogName)
            End If
            Return True
        Catch ex As Exception
            'Unable to Create Event Log
            'ex.Message()
            Return False
        Finally
            'Close the log.
            ev.Close()
        End Try
    End Function

End Module
