﻿Option Explicit On
Option Strict On

Imports System.ComponentModel

Module Globals

    Public CountryCode As Integer
    Public MCDBlibConfig As String

    Public Const RECEIPIENT_MARKETING_BIT_MASK As Integer = &H1
    Public Const RECEIPIENT_MONITOR_CENTER_BIT_MASK As Integer = &H2
    Public Const RECEIPIENT_APPROVING_BIT_MASK As Integer = &H4
    Public Const RECEIPIENT_MARKETING_RECEPTION_BIT_MASK As Integer = &H8

    Public Enum DocDestination As Integer
        Reception = 0
        MonitorCenter = 1
        Approving = 2
        KupatHolimClalit = 3
        Email = 4
        Marketing = 5
    End Enum

    Public Const RESULT_UNKNOWN As Integer = 0
    Public Const RESULT_SUCCESS As Integer = 1
    Public Const RESULT_SMARTPHONE_NOT_APPROVED As Integer = 901
    Public Const RESULT_EMPTY_PDF_DATA As Integer = 902
    Public Const RESULT_PDF_FILE_CORRUPTED As Integer = 903
    Public Const RESULT_DB_UPDATE_ERROR As Integer = 904
    Public Const RESULT_EMPTY_CUSTOMER_ID As Integer = 905
    Public Const RESULT_IMG_FILE_CORRUPTED As Integer = 906
    Public Const RESULT_INVALID_CUSTOMER_ID As Integer = 907

    Public Const RESULT_INVALID_DOCUMENT_TYPE As Integer = 950
    Public Const RESULT_INVALID_DOCUMENT_CONTENT As Integer = 960

    Public Enum DocumentType
        <Description(".bmp")> BMP
        <Description(".jpg")> JPG
        <Description(".gif")> GIF
        <Description(".tif")> TIF
        <Description(".png")> PNG
        <Description(".pdf")> PDF
        <Description(".???")> UNKNOWN
    End Enum

    Public Const WEB_APPLICATION_IDENTIFIER As String = "WEB"
    Public Const WEB_APPLICATION_SOURCE As Integer = 4

    Public Const WHATSAPP_APPLICATION_IDENTIFIER As String = "WHATSAPP"
    Public Const WHATSAPP_APPLICATION_SOURCE As Integer = 5

    Public Const DOCSCANNER_APPLICATION_SOURCE As Integer = 1
    Public Const MICU_APPLICATION_SOURCE As Integer = 3
    Public Const TZABAR_APPLICATION_SOURCE As Integer = 6
   

    Public Enum TechStatus
        AccountDisable = 0
        ShlTechnician = 1
        ShlSalesman = 2
        TigburEmployee = 3
        ShlMedicalStaff = 4
        TzabarRefuaEmployee = 5
        BiotronikEmployee = 6
        TeremEmployee = 7
        ShlMicuStaff = 8
    End Enum

End Module
