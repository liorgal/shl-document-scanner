﻿Option Explicit On
Option Strict Off

Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.ComponentModel

Imports System.Configuration
Imports System.Net.Configuration
Imports System.Web.Configuration


Module NetMail

    Public arrSendTo() As String
    Public arrSendCC() As String

    'For more information look at http://www.systemnetmail.com
    Public Function SendEmail(ByVal strBodyMessage As String, ByVal strSendTo As String) As Boolean

        Dim I As Integer
        Dim strTempString As String
        Dim strSmtpFrom As String = ""

        Try
            'Read SMTP Form
            Dim myConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Dim mailSettings As MailSettingsSectionGroup = myConfiguration.GetSectionGroup("system.net/mailSettings")
            If Not mailSettings Is Nothing Then
                strSmtpFrom = mailSettings.Smtp.From
            Else
                strSmtpFrom = My.Computer.Name.ToLower & "@" & System.Environment.UserDomainName & ".com"
            End If
            myConfiguration = Nothing
            mailSettings = Nothing

            'Create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            'mail.From = New MailAddress(My.Computer.Name & "@" & System.Environment.UserDomainName, My.Computer.Name)
            mail.From = New MailAddress(strSmtpFrom, My.Computer.Name)

            'Process Send TO
            arrSendTo = Split(strSendTo, ";")
            For I = 0 To UBound(arrSendTo)
                mail.To.Add(arrSendTo(I))
            Next

            'Process Send CC
            strTempString = My.Settings.EmailSendCC
            If strTempString.Trim <> "" Then
                arrSendCC = Split(strTempString, ";")
                For I = 0 To UBound(arrSendCC)
                    mail.CC.Add(arrSendCC(I))
                Next
            End If

            'Set the Subject
            mail.Subject = "SHL Document Scanner"

            'Set the Body
            mail.Body = strBodyMessage

            'Add Custom Header
            mail.Headers.Add("X-Application", My.Application.Info.ProductName)

            'specify the priority of the mail message
            mail.Priority = MailPriority.High

            ''Add Attachments
            'If File.Exists(My.Application.Info.DirectoryPath & "\" & EXCEL_FILE_NAME) Then
            '    mail.Attachments.Add(New Attachment(My.Application.Info.DirectoryPath & "\" & EXCEL_FILE_NAME))
            'End If
            'If File.Exists(My.Settings.AdditionalFileName) Then
            '    mail.Attachments.Add(New Attachment(My.Settings.AdditionalFileName))
            'End If

            'send the message
            Dim smtp As New SmtpClient()    'Initializes a new instance of the SmtpClient class by using configuration file settings
            smtp.Send(mail)

            'Release Mail Object
            mail.Attachments.Dispose()
            mail.Dispose()
            mail = Nothing
            smtp = Nothing

            Return True

        Catch Ex As Exception
            Dim Ex2 As Exception = Ex
            Dim errorMessage As String = String.Empty
            While Not (Ex2 Is Nothing)
                errorMessage += Ex2.ToString()
                Ex2 = Ex2.InnerException
            End While
            MsgBox(errorMessage, MsgBoxStyle.Critical, "SendEmail")
            Return False

        End Try
    End Function

    Public Function SendEmail(ByVal strMessageSubject As String, ByVal strMessageBody As String, ByVal strAttachmentFileName As String, ByVal strSendTo As String) As Boolean

        Dim I As Integer
        Dim strTempString As String
        Dim strSmtpFrom As String = ""

        Try
            'Read SMTP Settings Form Application.Config (EXE)
            'Dim myConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            'Dim mailSettings As MailSettingsSectionGroup = myConfiguration.GetSectionGroup("system.net/mailSettings")

            'Read SMTP Settings Form Web.Config (WEB)
            Dim myConfiguration As Configuration = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath)
            Dim mailSettings As MailSettingsSectionGroup = myConfiguration.GetSectionGroup("system.net/mailSettings")

            If Not mailSettings Is Nothing Then
                strSmtpFrom = mailSettings.Smtp.From
            Else
                strSmtpFrom = My.Computer.Name.ToLower & "@" & System.Environment.UserDomainName & ".com"
            End If
            myConfiguration = Nothing
            mailSettings = Nothing

            'Create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            mail.From = New MailAddress(strSmtpFrom, "Scanner Application")

            'Process Send TO
            strTempString = strSendTo
            arrSendTo = Split(strTempString, ";")
            For I = 0 To UBound(arrSendTo)
                mail.To.Add(arrSendTo(I))
            Next

            'Set the Subject
            mail.Subject = strMessageSubject

            'Set the Body
            mail.Body = strMessageBody

            'Add Custom Header
            mail.Headers.Add("X-Application", My.Application.Info.ProductName)

            'specify the priority of the mail message
            mail.Priority = MailPriority.High

            'Add Attachments
            If strAttachmentFileName.Trim <> "" Then
                If File.Exists(strAttachmentFileName) Then
                    mail.Attachments.Add(New Attachment(strAttachmentFileName))
                End If
            End If

            'send the message
            Dim smtp As New SmtpClient()    'Initializes a new instance of the SmtpClient class by using configuration file settings
            smtp.Send(mail)

            'Release Mail Object
            mail.Attachments.Dispose()
            mail.Dispose()
            mail = Nothing
            smtp = Nothing

            Return True

        Catch Ex As Exception
            Dim Ex2 As Exception = Ex
            Dim errorMessage As String = String.Empty
            While Not (Ex2 Is Nothing)
                errorMessage += Ex2.ToString()
                Ex2 = Ex2.InnerException
            End While
            MsgBox(errorMessage, MsgBoxStyle.Critical, "SendEmail")
            Return False

        End Try
    End Function

    Public Function SendTestEmail(ByVal strMailTo As String) As Boolean

        Dim I As Integer
        Dim strTempString As String
        Dim strSmtpFrom As String = ""

        Try
            'Read SMTP Form
            Dim myConfiguration As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Dim mailSettings As MailSettingsSectionGroup = myConfiguration.GetSectionGroup("system.net/mailSettings")
            If Not mailSettings Is Nothing Then
                strSmtpFrom = mailSettings.Smtp.From
            Else
                strSmtpFrom = My.Computer.Name.ToLower & "@" & System.Environment.UserDomainName & ".com"
            End If
            myConfiguration = Nothing
            mailSettings = Nothing

            'Create the mail message
            Dim mail As New MailMessage()

            'set the addresses
            'mail.From = New MailAddress(My.Computer.Name & "@" & System.Environment.UserDomainName, My.Computer.Name)
            mail.From = New MailAddress(strSmtpFrom, My.Computer.Name)

            'Process Send TO
            strTempString = strMailTo
            arrSendTo = Split(strTempString, ";")
            For I = 0 To UBound(arrSendTo)
                mail.To.Add(arrSendTo(I))
            Next

            'Set the Subject
            mail.Subject = "SHL Document Scanner Test Email"

            'Set the Body
            mail.Body = "This is a test message from SHL Document Scanner software." & vbNewLine & _
                        "Sended on:" & Date.Now.ToString

            'Add Custom Header
            mail.Headers.Add("X-Application", My.Application.Info.ProductName)

            'specify the priority of the mail message
            mail.Priority = MailPriority.High

            'send the message
            Dim smtp As New SmtpClient()    'Initializes a new instance of the SmtpClient class by using configuration file settings
            smtp.Send(mail)

            'Release Mail Object
            mail.Dispose()
            mail = Nothing
            smtp = Nothing

            Return True

        Catch Ex As Exception
            Dim Ex2 As Exception = Ex
            Dim errorMessage As String = String.Empty
            While Not (Ex2 Is Nothing)
                errorMessage += Ex2.ToString()
                Ex2 = Ex2.InnerException
            End While
            MsgBox(errorMessage, MsgBoxStyle.Critical, "SendTestEmail")
            Return False

        End Try
    End Function

#Region " Tests "

    Sub PlainText()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'PlainText

    Sub MultipleRecipients()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        'to specify a friendly 'from' name, we use a different ctor
        mail.From = New MailAddress("me@mycompany.com", "Steve James")

        'since the To,Cc, and Bcc accept addresses, we can use the same technique as the From address
        'since the To, Cc, and Bcc properties are collections, to add multiple addreses, we simply call .Add(...) multple times
        mail.To.Add("you@yourcompany.com")
        mail.To.Add("you2@yourcompany.com")
        mail.CC.Add("cc1@yourcompany.com")
        mail.CC.Add("cc2@yourcompany.com")
        mail.Bcc.Add("blindcc1@yourcompany.com")
        mail.Bcc.Add("blindcc2@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'MultipleRecipients

    Sub AttachmentFromFile1()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this content is in the body"

        'add an attachment from the filesystem
        mail.Attachments.Add(New Attachment("c:\temp\example.txt"))

        'to add additional attachments, simply call .Add(...) again
        mail.Attachments.Add(New Attachment("c:\temp\example2.txt"))
        mail.Attachments.Add(New Attachment("c:\temp\example3.txt"))

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'AttachmentFromFile

    Sub Authenticate1()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")

        'to authenticate we set the username and password properites on the SmtpClient
        smtp.Credentials = New NetworkCredential("username", "secret")
        smtp.Send(mail)
    End Sub 'Authenticate

    Sub MultiPartMime()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"

        'first we create the Plain Text part
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("This is my plain text content, viewable by those clients that don't support html", Nothing, "text/plain")
        'then we create the Html part
        Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<b>this is bold text, and viewable by those mail clients that support html</b>", Nothing, "text/html")
        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(htmlView)


        'send the message
        Dim smtp As New SmtpClient("127.0.0.1") 'specify the mail server address
        smtp.Send(mail)
    End Sub 'MultiPartMime

    Sub EmbedImages1()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"

        'first we create the Plain Text part
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("This is my plain text content, viewable by those clients that don't support html", Nothing, "text/plain")

        'then we create the Html part
        'to embed images, we need to use the prefix 'cid' in the img src value
        'the cid value will map to the Content-Id of a Linked resource.
        'thus <img src='cid:companylogo'> will map to a LinkedResource with a ContentId of 'companylogo'
        Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("Here is an embedded image.<img src=cid:companylogo>", Nothing, "text/html")

        'create the LinkedResource (embedded image)
        Dim logo As New LinkedResource("c:\temp\logo.gif")
        logo.ContentId = "companylogo"
        'add the LinkedResource to the appropriate view
        htmlView.LinkedResources.Add(logo)

        'add the views
        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(htmlView)


        'send the message
        Dim smtp As New SmtpClient("127.0.0.1") 'specify the mail server address
        smtp.Send(mail)
    End Sub 'EmbedImages

    Sub FriendlyNonAsciiName()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        'to specify a friendly non ascii name, we use a different ctor. 
        'A ctor that accepts an encoding that matches the text of the name
        mail.From = New MailAddress("me@mycompany.com", "Steve Øbirk", Encoding.GetEncoding("iso-8859-1"))
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'FriendlyNonAsciiName

    Sub SetPriority()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'specify the priority of the mail message
        mail.Priority = MailPriority.High

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'SetPriority

    Sub SetTheReplyToHeader()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'specify the priority of the mail message
        mail.ReplyTo = New MailAddress("SomeOtherAddress@mycompany.com")

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'SetTheReplyToHeader

    Sub ReadReceipts()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'To request a read receipt, we need add a custom header named 'Disposition-Notification-To'
        'in this example, read receipts will go back to 'someaddress@mydomain.com'
        'it's important to note that read receipts will only be sent by those mail clients that 
        'a) support them
        'and
        'b)have them enabled.
        mail.Headers.Add("Disposition-Notification-To", "<someaddress@mydomain.com>")


        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'ReadReceipts

    Sub CustomHeaders()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'to add custom headers, we use the Headers.Add(...) method to add headers to the 
        '.Headers collection
        mail.Headers.Add("X-Company", "My Company")
        mail.Headers.Add("X-Location", "Hong Kong")


        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'CustomHeaders

    Sub AttachmentFromFile()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this content is in the body"

        'add an attachment from the filesystem
        mail.Attachments.Add(New Attachment("c:\temp\example.txt"))

        'to add additional attachments, simply call .Add(...) again
        mail.Attachments.Add(New Attachment("c:\temp\example2.txt"))
        mail.Attachments.Add(New Attachment("c:\temp\example3.txt"))

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'AttachmentFromFile

    Sub AttachmentFromStream()

        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this content is in the body"

        'Get some binary data
        Dim data As Byte() = GetData()

        'save the data to a memory stream
        Dim ms As New MemoryStream(data)

        'create the attachment from a stream. Be sure to name the data with a file and 
        'media type that is respective of the data
        mail.Attachments.Add(New Attachment(ms, "example.txt", "text/plain"))

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'AttachmentFromStream

    Function GetData() As Byte()
        'this method just returns some binary data.
        'it could come from anywhere, such as Sql Server
        Dim s As String = "this is some text"
        Dim data As Byte() = Encoding.ASCII.GetBytes(s)
        Return data
    End Function 'GetData

    Function ReadConfig() As Boolean

        'Dim mMailSettings As System.Net.Configuration.MailSettingsSectionGroup

        'Dim mPort As Integer = mMailSettings.Smtp.Network.Port
        'Dim mHost As String = mMailSettings.Smtp.Network.Host
        'Dim mPassword As String = mMailSettings.Smtp.Network.Password
        'Dim mUsername As String = mMailSettings.Smtp.Network.Username
    End Function

    Sub Authenticate()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")

        'to authenticate we set the username and password properites on the SmtpClient
        smtp.Credentials = New NetworkCredential("username", "secret")
        smtp.Send(mail)
    End Sub 'Authenticate

    Sub EmbedImages()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"

        'first we create the Plain Text part
        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("This is my plain text content, viewable by those clients that don't support html", Nothing, "text/plain")

        'then we create the Html part
        'to embed images, we need to use the prefix 'cid' in the img src value
        'the cid value will map to the Content-Id of a Linked resource.
        'thus <img src='cid:companylogo'> will map to a LinkedResource with a ContentId of 'companylogo'
        Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("Here is an embedded image.<img src=cid:companylogo>", Nothing, "text/html")

        'create the LinkedResource (embedded image)
        Dim logo As New LinkedResource("c:\temp\logo.gif")
        logo.ContentId = "companylogo"
        'add the LinkedResource to the appropriate view
        htmlView.LinkedResources.Add(logo)

        'add the views
        mail.AlternateViews.Add(plainView)
        mail.AlternateViews.Add(htmlView)


        'send the message
        Dim smtp As New SmtpClient("127.0.0.1") 'specify the mail server address
        smtp.Send(mail)
    End Sub 'EmbedImages

    Sub SendAsync()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1") 'specify the mail server address
        'the userstate can be any object. The object can be accessed in the callback method
        'in this example, we will just use the MailMessage object.
        Dim userState As Object = mail

        'wire up the event for when the Async send is completed
        AddHandler smtp.SendCompleted, AddressOf SmtpClient_OnCompleted

        smtp.SendAsync(mail, userState)
    End Sub 'SendAsync

    Public Sub SmtpClient_OnCompleted(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        'Get the Original MailMessage object
        Dim mail As MailMessage = CType(e.UserState, MailMessage)

        'write out the subject
        Dim subject As String = mail.Subject

        If e.Cancelled Then
            Console.WriteLine("Send canceled for mail with subject [{0}].", subject)
        End If
        If Not (e.Error Is Nothing) Then
            Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString())
        Else
            Console.WriteLine("Message [{0}] sent.", subject)
        End If
    End Sub 'SmtpClient_OnCompleted

    Public Sub EmailWebPage()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"

        'screen scrape the html
        Dim html As String = ScreenScrapeHtml("http://localhost/example.htm")
        mail.Body = html
        mail.IsBodyHtml = True

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'EmailWebPage

    Public Function ScreenScrapeHtml(ByVal url As String) As String
        Dim objRequest As WebRequest = System.Net.HttpWebRequest.Create(url)
        Dim sr As New StreamReader(objRequest.GetResponse().GetResponseStream())
        Dim result As String = sr.ReadToEnd()
        sr.Close()
        Return result
    End Function 'ScreenScrapeHtml

    Public Sub NonAsciiMail()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("you@yourcompany.com")

        'set the content
        mail.Subject = "This is an email"

        'to send non-ascii content, we need to set the encoding that matches the 
        'string characterset.
        'In this example we use the ISO-8859-1 characterset
        mail.Body = "this text has some ISO-8859-1 characters: âÒÕÇ"
        mail.BodyEncoding = Encoding.GetEncoding("iso-8859-1")

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        smtp.Send(mail)
    End Sub 'NonAsciiMail 

    Public Sub InnerExceptions()
        'create the mail message
        Dim mail As New MailMessage()

        'set the addresses
        mail.From = New MailAddress("me@mycompany.com")
        mail.To.Add("him@hiscompany.com")

        'set the content
        mail.Subject = "This is an email"
        mail.Body = "this is the body content of the email."

        'send the message
        Dim smtp As New SmtpClient("127.0.0.1")
        Try
            smtp.Send(mail)
        Catch ex As Exception
            Dim ex2 As Exception = ex
            Dim errorMessage As String = String.Empty
            While Not (ex2 Is Nothing)
                errorMessage += ex2.ToString()
                ex2 = ex2.InnerException
            End While

            Console.WriteLine(errorMessage)
        End Try
    End Sub 'InnerExceptions

#End Region

End Module
