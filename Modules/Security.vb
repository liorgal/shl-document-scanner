﻿Option Explicit On
Option Strict On

Imports System.Security.Principal

Module Security

    'Use the standard logon provider for the system. 
    'The default security provider is negotiate, unless you pass NULL for the domain name and the user name 
    'is not in UPN format. In this case, the default provider is NTLM. 
    'NOTE: Windows 2000/NT:   The default security provider is NTLM.
    Private Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    'This logon type is intended for users who will be interactively using the computer, such as a user being logged on 
    'by a terminal server, remote shell, or similar process.
    'This logon type has the additional expense of caching logon information for disconnected operations; 
    'therefore, it is inappropriate for some client/server applications,
    'such as a mail server.
    Private Const LOGON32_LOGON_INTERACTIVE As Integer = 2

    'This logon type is intended for high performance servers to authenticate plaintext passwords.
    'The LogonUser function does not cache credentials for this logon type.
    Private Const LOGON32_LOGON_NETWORK As Integer = 3

    'This logon type is intended for batch servers, where processes may be executing on behalf of a user without 
    'their direct intervention. This type is also for higher performance servers that process many plaintext
    'authentication attempts at a time, such as mail or Web servers. 
    'The LogonUser function does not cache credentials for this logon type.
    Private Const LOGON32_LOGON_BATCH As Integer = 4

    'Indicates a service-type logon. The account provided must have the service privilege enabled.
    Private Const LOGON32_LOGON_SERVICE As Integer = 5

    'This logon type is for GINA DLLs that log on users who will be interactively using the computer. 
    'This logon type can generate a unique audit record that shows when the workstation was unlocked. 
    Private Const LOGON32_LOGON_UNLOCK As Integer = 7

    'This logon type preserves the name and password in the authentication package, which allows the server to make 
    'connections to other network servers while impersonating the client. A server can accept plaintext credentials 
    'from a client, call LogonUser, verify that the user can access the system across the network, and still 
    'communicate with other servers.
    'NOTE: Windows NT:  This value is not supported. 
    Private Const LOGON32_LOGON_NETWORK_CLEARTEXT As Integer = 8

    'This logon type allows the caller to clone its current token and specify new credentials for outbound connections.
    'The new logon session has the same local identifier but uses different credentials for other network connections. 
    'NOTE: This logon type is supported only by the LOGON32_PROVIDER_WINNT50 logon provider.
    'NOTE: Windows NT:  This value is not supported. 
    Private Const LOGON32_LOGON_NEW_CREDENTIALS As Integer = 9

    Private ImpersonationContext As WindowsImpersonationContext

    Private Declare Function LogonUserA Lib "advapi32.dll" ( _
                            ByVal lpszUsername As String, _
                            ByVal lpszDomain As String, _
                            ByVal lpszPassword As String, _
                            ByVal dwLogonType As Integer, _
                            ByVal dwLogonProvider As Integer, _
                            ByRef phToken As IntPtr) As Integer

    Private Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
                            ByVal ExistingTokenHandle As IntPtr, _
                            ByVal ImpersonationLevel As Integer, _
                            ByRef DuplicateTokenHandle As IntPtr) As Integer

    Private Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Long
    Private Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Long


    ' NOTE:
    ' The identity of the process that impersonates a specific user on a thread must have 
    ' "Act as part of the operating system" privilege. If the the Aspnet_wp.exe process runs
    ' under a the ASPNET account, this account does not have the required privileges to 
    ' impersonate a specific user. This information applies only to the .NET Framework 1.0. 
    ' This privilege is not required for the .NET Framework 1.1.
    '
    ' Sample call:
    '
    '    If impersonateValidUser("username", "domain", "password") Then
    '        'Insert your code here.
    '
    '        undoImpersonation()
    '    Else
    '        'Impersonation failed. Include a fail-safe mechanism here.
    '    End If
    '
    Private Function ImpersonateValidUser(ByVal strUserName As String, ByVal strDomain As String, ByVal strPassword As String) As Boolean

        'http://www.experts-exchange.com/Microsoft/Development/.NET/Q_23630236.html
        'http://www.dreamincode.net/forums/topic/108595-copy-file-using-web-service/
        'http://kellychronicles.spaces.live.com/blog/cns!A0D71E1614E8DBF8!467.entry
        'http://www.codeproject.com/KB/webservices/Web_Method_To_Restart_NT.aspx

        Try
            Dim lRetVal As Long
            Dim token As IntPtr = IntPtr.Zero
            Dim tokenDuplicate As IntPtr = IntPtr.Zero
            Dim tempWindowsIdentity As WindowsIdentity

            ImpersonateValidUser = False

            If RevertToSelf() <> 0 Then

                'http://blogs.msdn.com/b/shawnfa/archive/2005/03/21/400088.aspx
                'If LogonUserA(strUserName, strDomain, strPassword, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, token) <> 0 Then

                If LogonUserA(strUserName, strDomain, strPassword, LOGON32_LOGON_NETWORK, LOGON32_PROVIDER_DEFAULT, token) <> 0 Then

                    If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then

                        tempWindowsIdentity = New WindowsIdentity(tokenDuplicate)
                        ImpersonationContext = tempWindowsIdentity.Impersonate()

                        If Not (ImpersonationContext Is Nothing) Then
                            ImpersonateValidUser = True
                        End If
                    End If
                End If
            End If

            If Not tokenDuplicate.Equals(IntPtr.Zero) Then
                lRetVal = CloseHandle(tokenDuplicate)
            End If

            If Not token.Equals(IntPtr.Zero) Then
                lRetVal = CloseHandle(token)
            End If

        Catch Ex As Exception
            Call Debug.Print(Ex.Message)
        End Try

    End Function

    Public Sub UndoImpersonation()
        Try
            If Not (ImpersonationContext Is Nothing) Then
                ImpersonationContext.Undo()
            End If
        Catch Ex As Exception
            Call Debug.Print(Ex.Message)
        End Try
    End Sub

    Public Sub ImpersonateAdmin()
        If My.Settings.UserName.Trim <> "" Then
            Dim bRetVal As Boolean = ImpersonateValidUser(My.Settings.UserName, My.Settings.Domain, My.Settings.Password)
        End If
    End Sub

End Module
