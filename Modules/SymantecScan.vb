﻿Option Explicit On
Option Strict On

Imports System.IO

Module SymantecScan

    'Log Location
    'AV logs are stored in the following locations, depending on the version and operating system:
    '
    'Endpoint Protection 14.0
    '   Windows XP/Windows 2003 - \Documents and Settings\All Users\Application Data\Symantec\Symantec Endpoint Protection\Logs\AV
    '   Other Windows - \ProgramData\Symantec\Symantec Endpoint Protection\CurrentVersion\Data\Logs\AV
    '
    'Endpoint Protection 12.1    
    '   Windows XP /Windows 2003- \Documents and Settings\All Users\Application Data\Symantec\Symantec Endpoint Protection\Logs\AV
    '   Other Windows - \ProgramData\Symantec\Symantec Endpoint Protection\CurrentVersion\Data\Logs\AV
    '
    'Endpoint Protection 11
    '   \Documents and Settings\All Users\Application Data\Symantec\Symantec Endpoint Protection\Logs\AV
    '
    'SEP Linux AV log files are kept in /var/symantec/Logs

#Region "Test Functionas"

    Private Sub PerformScanning(ByVal strfiles As String(), ByVal strFilePath As String, ByVal strDestPath As String)

        'https://support.symantec.com/en_US/article.TECH104287.html
        'https://www.symantec.com/connect/articles/new-features-doscanexe-sep-ru6mp1
        'https://www.symantec.com/connect/articles/doscanexe-sep-antivirus-scans-command-prompt-introduction
        'https://support.symantec.com/en_US/article.TECH100099.html

        Try

            Dim myProcess As Process
            'Dim objFileStatus As FileStatus
            myProcess = New Process()
            If Not Directory.Exists(strFilePath & Convert.ToString("\Logs")) Then
                Directory.CreateDirectory(strFilePath & Convert.ToString("\Logs"))
            End If

            For Each strFile As String In strfiles
                'objFileStatus = New FileStatus()
                myProcess.StartInfo.FileName = "C:\Program Files\Symantec\Symantec Endpoint Protection\DoScan.exe"

                Dim myProCarg As String = ""

                'Only for Old Version Up to SEP 11 RU5 and bellow
                'myProCarg = " /cmdlinescan " & strFile & " /Logfile=""" & strFilePath & "\Logs\" & Path.GetFileName(strFile) & ".log"""

                'Only for New Version from SEP 12.1 and above
                myProCarg = " /ScanFile " & """ & strFile & """

                myProcess.StartInfo.Arguments = myProCarg
                myProcess.Start()
                myProcess.WaitForExit()

                Dim j As Integer = 0
                Dim y As Integer = 0
                For j = 0 To 1000000
                    y = y + 1
                Next

                Dim strFileName As String = (strFilePath & Convert.ToString("\Logs\")) + Path.GetFileName(strFile) + ".log"
                Dim strSearchLine As String = Nothing
                Dim swIsClean As Boolean = True
                Dim strMyFound As String = Nothing

                Dim objStreamReader As StreamReader = Nothing
                objStreamReader = File.OpenText(strFileName)

                While objStreamReader.Peek() <> -1
                    strSearchLine = objStreamReader.ReadLine()
                    If strSearchLine.Contains("Found") Then
                        swIsClean = False
                        strMyFound = strSearchLine
                    End If
                End While
                objStreamReader.Close()


                If swIsClean Then
                    File.Move(strFile, (strDestPath & Convert.ToString("\")) + Path.GetFileName(strFile))
                    If File.Exists(strFilePath & "\" & strFile) Then
                        File.Delete(strFilePath & "\" & strFile)
                    End If
                    'objFileStatus.UpdateUploadedFileStatus("Sucess", "Y", (strDestPath & Convert.ToString("\")) + Path.GetFileName(strFile), Path.GetFileNameWithoutExtension(strFile).Substring(6, Path.GetFileNameWithoutExtension(strFile).Length - 6), "No threat detected")
                    Console.WriteLine((Convert.ToString("File:") & strFile) + "Scanned Sucessfully")

                    File.Delete((strFilePath & Convert.ToString("\Logs\")) + Path.GetFileName(strFile) + ".log")
                Else
                    Dim path1 As String = strFile
                    File.Delete(path1)
                    'objFileStatus.UpdateUploadedFileStatus("Failure", "N", strFile, Path.GetFileNameWithoutExtension(strFile).Substring(6, Path.GetFileNameWithoutExtension(strFile).Length - 6), "Virus Found")

                    Console.WriteLine((Convert.ToString("File:") & path1) + "Deleted.")
                End If
            Next

        Catch Ex As Exception
            Console.Write("The Process Cannot be completed due to some exception")

        End Try

    End Sub

    Private Sub Test(ByVal strFilePath As String, ByVal strFileNames As String)

        Try
            'do av check here
            Dim myProcess As New Process()

            'address of command line virus scan exe
            myProcess.StartInfo.FileName = "C:\Program Files\Symantec\Symantec Endpoint Protection\SymCorpUI.exe"

            Dim myprocarg As String = "/SCAN=" + strFileNames + " /REPORT=C:\Upload\Temp\Report.txt"
            myProcess.StartInfo.Arguments = myprocarg
            myProcess.StartInfo.UseShellExecute = False
            myProcess.StartInfo.RedirectStandardInput = True
            myProcess.StartInfo.RedirectStandardOutput = True
            myProcess.StartInfo.RedirectStandardError = True
            myProcess.Start()
            Dim inputWriter As StreamWriter = myProcess.StandardInput
            Dim outputReader As StreamReader = myProcess.StandardOutput
            Dim errorReader As StreamReader = myProcess.StandardError

            myProcess.WaitForExit()
            myProcess.Close()

            'add some time for report to be written to file
            Dim j As Integer = 0
            Dim y As Integer = 0

            For j = 0 To 1000000
                y = y + 1
            Next

            Dim IsClean As Boolean = True

            'Get a StreamReader class that can be used to read the file
            Dim objStreamReader As StreamReader = Nothing
            objStreamReader = File.OpenText("C:\Upload\Temp\Report.txt")
            If Not objStreamReader.ReadToEnd().Contains("Found infections  :  0") Then
                IsClean = False
                File.Delete(strFilePath + "\" + strFileNames)
            End If
            objStreamReader.Close()


            If IsClean Then
                '?
            Else
                '?
            End If

        Catch generatedExceptionName As Exception
            Dim IsClean As Boolean = True
            MsgBox("Some Error While Attaching Your File")
            IsClean = False

            File.Delete(strFilePath + "\" + strFileNames)
        End Try
    End Sub

    Private Sub ConvertAVLogFile()

        'If (Cells(1, 1) <> "Time") Then

        '    REM ---   Add 1 to every index as our indexes are 0 based ---

        '    Cells(1, 0 + 1).Value = "Time"
        '    Cells(1, 1 + 1).Value = "Event"
        '    Cells(1, 2 + 1).Value = "Category"
        '    Cells(1, 3 + 1).Value = "Logger"
        '    Cells(1, 4 + 1).Value = "Computer"
        '    Cells(1, 5 + 1).Value = "User"
        '    Cells(1, 6 + 1).Value = "Virus"
        '    Cells(1, 7 + 1).Value = "File"
        '    Cells(1, 8 + 1).Value = "Wanted Action 1"
        '    Cells(1, 9 + 1).Value = "Wanted Action 2"
        '    Cells(1, 10 + 1).Value = "Real Action"
        '    Cells(1, 11 + 1).Value = "Virus Type"
        '    Cells(1, 12 + 1).Value = "Flags"
        '    Cells(1, 13 + 1).Value = "Description"
        '    Cells(1, 14 + 1).Value = "ScanID"
        '    Cells(1, 15 + 1).Value = "New_Ext"
        '    Cells(1, 16 + 1).Value = "Group ID"
        '    Cells(1, 17 + 1).Value = "Event Data"
        '    Cells(1, 18 + 1).Value = "VBin_ID"
        '    Cells(1, 19 + 1).Value = "Virus ID"
        '    Cells(1, 20 + 1).Value = "Quarantine Forward Status"
        '    Cells(1, 21 + 1).Value = "Access"
        '    Cells(1, 22 + 1).Value = "SND_Status"
        '    Cells(1, 23 + 1).Value = "Compressed"
        '    Cells(1, 24 + 1).Value = "Depth"
        '    Cells(1, 25 + 1).Value = "Still Infected"
        '    Cells(1, 26 + 1).Value = "Def Info"
        '    Cells(1, 27 + 1).Value = "Def Sequence Number"
        '    Cells(1, 28 + 1).Value = "Clean Info"
        '    Cells(1, 29 + 1).Value = "Delete Info"
        '    Cells(1, 30 + 1).Value = "Backup ID"
        '    Cells(1, 31 + 1).Value = "Parent"
        '    Cells(1, 32 + 1).Value = "GUID"
        '    Cells(1, 33 + 1).Value = "Client Group"
        '    Cells(1, 34 + 1).Value = "Address"
        '    Cells(1, 35 + 1).Value = "Domain Name"
        '    Cells(1, 36 + 1).Value = "NT Domain"
        '    Cells(1, 37 + 1).Value = "MAC Address"
        '    Cells(1, 38 + 1).Value = "Version"
        '    Cells(1, 39 + 1).Value = "Remote Machine"
        '    Cells(1, 40 + 1).Value = "Remote Machine IP"
        '    Cells(1, 41 + 1).Value = "Action 1 Status"
        '    Cells(1, 42 + 1).Value = "Action 2 Status"
        '    Cells(1, 43 + 1).Value = "License Feature Name"
        '    Cells(1, 44 + 1).Value = "License Feature Version"
        '    Cells(1, 45 + 1).Value = "License Serial Number"
        '    Cells(1, 46 + 1).Value = "License Fulfillment ID"
        '    Cells(1, 47 + 1).Value = "License Start Date"
        '    Cells(1, 48 + 1).Value = "License Expiration Date"
        '    Cells(1, 49 + 1).Value = "License LifeCycle"
        '    Cells(1, 50 + 1).Value = "License Seats Total"
        '    Cells(1, 51 + 1).Value = "License Seats"
        '    Cells(1, 52 + 1).Value = "Error Code"
        '    Cells(1, 53 + 1).Value = "License Seats Delta"
        '    Cells(1, 54 + 1).Value = "Status"
        '    Cells(1, 55 + 1).Value = "Domain GUID"
        '    Cells(1, 56 + 1).Value = "Log Session GUID"
        '    Cells(1, 57 + 1).Value = "VBin Session ID"
        '    Cells(1, 58 + 1).Value = "Login Domain"
        '    Cells(1, 59 + 1).Value = "Event Data 2"
        '    Cells(1, 60 + 1).Value = "Eraser Category ID"
        '    Cells(1, 61 + 1).Value = "Dynamic Categoryset ID"
        '    Cells(1, 62 + 1).Value = "Dynamic Subcategoryset ID"
        '    Cells(1, 63 + 1).Value = "Display Name To Use"
        '    Cells(1, 64 + 1).Value = "Reputation Disposition"
        '    Cells(1, 65 + 1).Value = "Reputation Confidence"
        '    Cells(1, 66 + 1).Value = "First Seen"
        '    Cells(1, 67 + 1).Value = "Reputation Prevalence"
        '    Cells(1, 68 + 1).Value = "Downloaded URL"
        '    Cells(1, 69 + 1).Value = "Creator For Dropper"
        '    Cells(1, 70 + 1).Value = "CIDS State"
        '    Cells(1, 71 + 1).Value = "Behavior Risk Level"
        '    Cells(1, 72 + 1).Value = "Detection Type"
        '    Cells(1, 73 + 1).Value = "Acknowledge Text"
        '    Cells(1, 74 + 1).Value = "VSIC State"
        '    Cells(1, 75 + 1).Value = "Scan GUID"
        '    Cells(1, 76 + 1).Value = "Scan Duration"
        '    Cells(1, 77 + 1).Value = "Scan Start Time"
        '    Cells(1, 78 + 1).Value = "TargetApp Type"
        '    Cells(1, 79 + 1).Value = "Scan Command GUID"
        'End If

        'ActiveSheet.Rows(1).EntireRow.Font.Size = 16

        'totalrows = ActiveSheet.UsedRange.Rows.Count

        'For currentRow = 2 To totalrows

        '    REM  ---- Convert time -----
        '    TimeString = Cells(currentRow, "A").Value

        '    YearVal = CInt("&H" & Mid(TimeString, 1, 2)) + 1970
        '    MonthVal = CInt("&H" & Mid(TimeString, 3, 2)) + 1
        '    DayVal = CInt("&H" & Mid(TimeString, 5, 2))
        '    HourVal = CInt("&H" & Mid(TimeString, 7, 2))
        '    MinuteVal = CInt("&H" & Mid(TimeString, 9, 2))
        '    SecondVal = CInt("&H" & Mid(TimeString, 11, 2))

        '    Cells(currentRow, "A").Value = DateSerial(YearVal, MonthVal, DayVal) + TimeSerial(HourVal, MinuteVal, SecondVal)
        '    Cells(currentRow, "A").NumberFormat = "mm/dd/yyyy hh:mm:ss"

        '    REM  ---- Description ----

        '    If Cells(currentRow, "D").Value = 2 And Cells(currentRow, "BM").Value = 1 Then
        '        Cells(currentRow, "N").Value = "AP realtime deferred scanning"
        '    End If

        '    REM  ---- Convert Event -----

        '    Select Case Cells(currentRow, "B").Value
        '        Case 1
        '            Cells(currentRow, "B").Value = "GL_EVENT_IS_ALERT"
        '        Case 2
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_STOP"
        '        Case 3
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_START"
        '        Case 4
        '            Cells(currentRow, "B").Value = "GL_EVENT_PATTERN_UPDATE"
        '        Case 5
        '            Cells(currentRow, "B").Value = "GL_EVENT_INFECTION"
        '        Case 6
        '            Cells(currentRow, "B").Value = "GL_EVENT_FILE_NOT_OPEN"
        '        Case 7
        '            Cells(currentRow, "B").Value = "GL_EVENT_LOAD_PATTERN"
        '        Case 8
        '            Cells(currentRow, "B").Value = "GL_STD_MESSAGE_INFO"
        '        Case 9
        '            Cells(currentRow, "B").Value = "GL_STD_MESSAGE_ERROR"
        '        Case 10
        '            Cells(currentRow, "B").Value = "GL_EVENT_CHECKSUM"
        '        Case 11
        '            Cells(currentRow, "B").Value = "GL_EVENT_TRAP"
        '        Case 12
        '            Cells(currentRow, "B").Value = "GL_EVENT_CONFIG_CHANGE"
        '        Case 13
        '            Cells(currentRow, "B").Value = "GL_EVENT_SHUTDOWN"
        '        Case 14
        '            Cells(currentRow, "B").Value = "GL_EVENT_STARTUP"
        '        Case 16
        '            Cells(currentRow, "B").Value = "GL_EVENT_PATTERN_DOWNLOAD"
        '        Case 17
        '            Cells(currentRow, "B").Value = "GL_EVENT_TOO_MANY_VIRUSES"
        '        Case 18
        '            Cells(currentRow, "B").Value = "GL_EVENT_FWD_TO_QSERVER"
        '        Case 19
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCANDLVR"
        '        Case 20
        '            Cells(currentRow, "B").Value = "GL_EVENT_BACKUP"
        '        Case 21
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_ABORT"
        '        Case 22
        '            Cells(currentRow, "B").Value = "GL_EVENT_RTS_LOAD_ERROR"
        '        Case 23
        '            Cells(currentRow, "B").Value = "GL_EVENT_RTS_LOAD"
        '        Case 24
        '            Cells(currentRow, "B").Value = "GL_EVENT_RTS_UNLOAD"
        '        Case 25
        '            Cells(currentRow, "B").Value = "GL_EVENT_REMOVE_CLIENT"
        '        Case 26
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_DELAYED"
        '        Case 27
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_RESTART"
        '        Case 28
        '            Cells(currentRow, "B").Value = "GL_EVENT_ADD_SAVROAMCLIENT_TOSERVER"
        '        Case 29
        '            Cells(currentRow, "B").Value = "GL_EVENT_REMOVE_SAVROAMCLIENT_FROMSERVER"
        '        Case 30
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_WARNING"
        '        Case 31
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_ERROR"
        '        Case 32
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_GRACE"
        '        Case 33
        '            Cells(currentRow, "B").Value = "GL_EVENT_UNAUTHORIZED_COMM"
        '        Case 34
        '            Cells(currentRow, "B").Value = "GL_EVENT_LOG_FWD_THRD_ERR"
        '        Case 35
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_INSTALLED"
        '        Case 36
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_ALLOCATED"
        '        Case 37
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_OK"
        '        Case 38
        '            Cells(currentRow, "B").Value = "GL_EVENT_LICENSE_DEALLOCATED"
        '        Case 39
        '            Cells(currentRow, "B").Value = "GL_EVENT_BAD_DEFS_ROLLBACK"
        '        Case 40
        '            Cells(currentRow, "B").Value = "GL_EVENT_BAD_DEFS_UNPROTECTED"
        '        Case 41
        '            Cells(currentRow, "B").Value = "GL_EVENT_SAV_PROVIDER_PARSING_ERROR"
        '        Case 42
        '            Cells(currentRow, "B").Value = "GL_EVENT_RTS_ERROR"
        '        Case 43
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMPLIANCE_FAIL"
        '        Case 44
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMPLIANCE_SUCCESS"
        '        Case 45
        '            Cells(currentRow, "B").Value = "GL_EVENT_SECURITY_SYMPROTECT_POLICYVIOLATION"
        '        Case 46
        '            Cells(currentRow, "B").Value = "GL_EVENT_ANOMALY_START"
        '        Case 47
        '            Cells(currentRow, "B").Value = "GL_EVENT_DETECTION_ACTION_TAKEN"
        '        Case 48
        '            Cells(currentRow, "B").Value = "GL_EVENT_REMEDIATION_ACTION_PENDING"
        '        Case 49
        '            Cells(currentRow, "B").Value = "GL_EVENT_REMEDIATION_ACTION_FAILED"
        '        Case 50
        '            Cells(currentRow, "B").Value = "GL_EVENT_REMEDIATION_ACTION_SUCCESSFUL"
        '        Case 51
        '            Cells(currentRow, "B").Value = "GL_EVENT_ANOMALY_FINISH"
        '        Case 52
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_LOGIN_FAILED"
        '        Case 53
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_LOGIN_SUCCESS"
        '        Case 54
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_UNAUTHORIZED_COMM"
        '        Case 55
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_INSTALL_AV"
        '        Case 56
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_INSTALL_FW"
        '        Case 57
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_UNINSTALL"
        '        Case 58
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_UNINSTALL_ROLLBACK"
        '        Case 59
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_SERVER_GROUP_ROOT_CERT_ISSUE"
        '        Case 60
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_SERVER_CERT_ISSUE"
        '        Case 61
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_TRUSTED_ROOT_CHANGE"
        '        Case 62
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMMS_SERVER_CERT_STARTUP_FAILED"
        '        Case 63
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_CHECKIN"
        '        Case 64
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_NO_CHECKIN"
        '        Case 65
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_SUSPENDED"
        '        Case 66
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_RESUMED"
        '        Case 67
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_DURATION_INSUFFICIENT"
        '        Case 68
        '            Cells(currentRow, "B").Value = "GL_EVENT_CLIENT_MOVE"
        '        Case 69
        '            Cells(currentRow, "B").Value = "GL_EVENT_SCAN_FAILED_ENHANCED"
        '        Case 70
        '            Cells(currentRow, "B").Value = "GL_EVENT_COMPLIANCE_FAILEDAUDIT"
        '        Case 71
        '            Cells(currentRow, "B").Value = "GL_EVENT_HEUR_THREAT_NOW_WHITELISTED"
        '        Case 71
        '            Cells(currentRow, "B").Value = "GL_EVENT_HEUR_THREAT_NOW_WHITELISTED"
        '        Case 72
        '            Cells(currentRow, "B").Value = "GL_EVENT_INTERESTING_PROCESS_DETECTED_START"
        '        Case 73
        '            Cells(currentRow, "B").Value = "GL_EVENT_LOAD_ERROR_BASH"
        '        Case 74
        '            Cells(currentRow, "B").Value = "GL_EVENT_LOAD_ERROR_BASH_DEFINITIONS"
        '        Case 75
        '            Cells(currentRow, "B").Value = "GL_EVENT_INTERESTING_PROCESS_DETECTED_FINISH"
        '        Case 76
        '            Cells(currentRow, "B").Value = "GL_EVENT_BASH_NOT_SUPPORTED_FOR_OS"
        '        Case 77
        '            Cells(currentRow, "B").Value = "GL_EVENT_HEUR_THREAT_NOW_KNOWN"
        '        Case 78
        '            Cells(currentRow, "B").Value = "GL_EVENT_DISABLE_BASH"
        '        Case 79
        '            Cells(currentRow, "B").Value = "GL_EVENT_ENABLE_BASH"
        '        Case 80
        '            Cells(currentRow, "B").Value = "GL_EVENT_DEFS_LOAD_FAILED"
        '        Case 81
        '            Cells(currentRow, "B").Value = "GL_EVENT_LOCALREP_CACHE_SERVER_ERROR"
        '        Case 82
        '            Cells(currentRow, "B").Value = "GL_EVENT_REPUTATION_CHECK_TIMEOUT"
        '        Case 83
        '            Cells(currentRow, "B").Value = "GL_EVENT_SYMEPSECFILTER_DRIVER_ERROR"
        '        Case 84
        '            Cells(currentRow, "B").Value = "GL_EVENT_VSIC_COMMUNICATION_WARNING"
        '        Case 85
        '            Cells(currentRow, "B").Value = "GL_EVENT_VSIC_COMMUNICATION_RESTORED"
        '        Case 86
        '            Cells(currentRow, "B").Value = "GL_EVENT_ELAM_LOAD_FAILED"
        '        Case 87
        '            Cells(currentRow, "B").Value = "GL_EVENT_ELAM_INVALID_OS"
        '        Case 88
        '            Cells(currentRow, "B").Value = "GL_EVENT_ELAM_ENABLE"
        '        Case 89
        '            Cells(currentRow, "B").Value = "GL_EVENT_ELAM_DISABLE"
        '        Case 90
        '            Cells(currentRow, "B").Value = "GL_EVENT_ELAM_BAD"
        '        Case 91
        '            Cells(currentRow, "B").Value = "GL_EVENT_ELAM_BAD_REPORTED_AS_UNKNOWN"
        '        Case 92
        '            Cells(currentRow, "B").Value = "GL_EVENT_DISABLE_SYMPROTECT"
        '        Case 93
        '            Cells(currentRow, "B").Value = "GL_EVENT_ENABLE_SYMPROTECT"
        '        Case 94
        '            Cells(currentRow, "B").Value = "GL_EVENT_NETSEC_EOC_PARSE_FAILED"
        '    End Select

        '    REM  ---- Convert Category -----

        '    Select Case Cells(currentRow, "C").Value
        '        Case 1
        '            Cells(currentRow, "C").Value = "GL_CAT_INFECTION"
        '        Case 2
        '            Cells(currentRow, "C").Value = "GL_CAT_SUMMARY"
        '        Case 3
        '            Cells(currentRow, "C").Value = "GL_CAT_PATTERN"
        '        Case 4
        '            Cells(currentRow, "C").Value = "GL_CAT_SECURITY"
        '    End Select

        '    REM  ---- Convert Logger -----

        '    Select Case Cells(currentRow, "D").Value
        '        Case 0
        '            Cells(currentRow, "D").Value = "LOGGER_Scheduled"
        '        Case 1
        '            Cells(currentRow, "D").Value = "LOGGER_Manual"
        '        Case 2
        '            Cells(currentRow, "D").Value = "LOGGER_Real_Time"
        '        Case 3
        '            Cells(currentRow, "D").Value = "LOGGER_Integrity_Shield"
        '        Case 6
        '            Cells(currentRow, "D").Value = "LOGGER_Console"
        '        Case 7
        '            Cells(currentRow, "D").Value = "LOGGER_VPDOWN"
        '        Case 8
        '            Cells(currentRow, "D").Value = "LOGGER_System"
        '        Case 9
        '            Cells(currentRow, "D").Value = "LOGGER_Startup"
        '        Case 10
        '            Cells(currentRow, "D").Value = "LOGGER_Idle"
        '        Case 11
        '            Cells(currentRow, "D").Value = "LOGGER_DefWatch"
        '        Case 12
        '            Cells(currentRow, "D").Value = "LOGGER_Licensing"
        '        Case 13
        '            Cells(currentRow, "D").Value = "LOGGER_Manual_Quarantine"
        '        Case 14
        '            Cells(currentRow, "D").Value = "LOGGER_SymProtect"
        '        Case 15
        '            Cells(currentRow, "D").Value = "LOGGER_Reboot_Processing"
        '        Case 16
        '            Cells(currentRow, "D").Value = "LOGGER_Bash"
        '        Case 17
        '            Cells(currentRow, "D").Value = "LOGGER_SymElam"
        '        Case 18
        '            Cells(currentRow, "D").Value = "LOGGER_PowerEraser"
        '        Case 19
        '            Cells(currentRow, "D").Value = "LOGGER_EOCScan"
        '        Case 100
        '            Cells(currentRow, "D").Value = "LOGGER_LOCAL_END"
        '        Case 101
        '            Cells(currentRow, "D").Value = "LOGGER_Client"
        '        Case 102
        '            Cells(currentRow, "D").Value = "LOGGER_Forwarded"
        '        Case 256
        '            Cells(currentRow, "D").Value = "LOGGER_Transport_Client"
        '    End Select

        '    REM  ---- Convert Wanted Action 1 -----

        '    Select Case Cells(currentRow, "I").Value
        '        Case 4294967295.0#
        '            Cells(currentRow, "I").Value = "Invalid"
        '        Case 1
        '            Cells(currentRow, "I").Value = "AC_MOVE - Quarantine"
        '        Case 2
        '            Cells(currentRow, "I").Value = "AC_RENAME - Rename"
        '        Case 3
        '            Cells(currentRow, "I").Value = "AC_DEL - Delete"
        '        Case 4
        '            Cells(currentRow, "I").Value = "AC_NOTHING - Leave Alone"
        '        Case 5
        '            Cells(currentRow, "I").Value = "AC_CLEAN - Clean"
        '        Case 6
        '            Cells(currentRow, "I").Value = "AC_REMOVE_MACROS - Remove Macros"
        '        Case 7
        '            Cells(currentRow, "I").Value = "AC_SAVE_AS - Save file as..."
        '        Case 8
        '            Cells(currentRow, "I").Value = "AC_SEND_TO_INTEL - Sent to backend"
        '        Case 9
        '            Cells(currentRow, "I").Value = "AC_MOVE_BACK - Restore from Quarantine"
        '        Case 10
        '            Cells(currentRow, "I").Value = "AC_RENAME_BACK - Rename Back (unused)"
        '        Case 11
        '            Cells(currentRow, "I").Value = "AC_UNDO - Undo Action"
        '        Case 12
        '            Cells(currentRow, "I").Value = "AC_BAD - Error"
        '        Case 13
        '            Cells(currentRow, "I").Value = "AC_BACKUP - Backup to quarantine (backup view)"
        '        Case 14
        '            Cells(currentRow, "I").Value = "AC_PENDING - Pending Analysis"
        '        Case 15
        '            Cells(currentRow, "I").Value = "AC_PARTIAL - Partially Fixed"
        '        Case 16
        '            Cells(currentRow, "I").Value = "AC_TERMINATE - Terminate Process Required"
        '        Case 17
        '            Cells(currentRow, "I").Value = "AC_EXCLUDE - Exclude from Scanning"
        '        Case 18
        '            Cells(currentRow, "I").Value = "AC_REBOOT_PROCESSING - Reboot Processing"
        '        Case 19
        '            Cells(currentRow, "I").Value = "AC_CLEANBYDELETE - Clean by Deletion"
        '        Case 20
        '            Cells(currentRow, "I").Value = "AC_ACCESSDENIED - Access Denied"
        '        Case 21
        '            Cells(currentRow, "I").Value = "AC_TERMINATE_PROCESS_ONLY"
        '        Case 22
        '            Cells(currentRow, "I").Value = "AC_NO_REPAIR"
        '        Case 23
        '            Cells(currentRow, "I").Value = "AC_FAIL"
        '        Case 24
        '            Cells(currentRow, "I").Value = "AC_RUN_POWERTOOL"
        '        Case 25
        '            Cells(currentRow, "I").Value = "AC_NO_REPAIR_POWERTOOL"
        '        Case 110
        '            Cells(currentRow, "I").Value = "AC_INTERESTING_PROCESS_CAL"
        '        Case 111
        '            Cells(currentRow, "I").Value = "AC_INTERESTING_PROCESS_DETECTED"
        '        Case 1000
        '            Cells(currentRow, "I").Value = "AC_INTERESTING_PROCESS_HASHED_DETECTED"
        '        Case 1001
        '            Cells(currentRow, "I").Value = "AC_DNS_HOST_FILE_EXCEPTION"

        '    End Select

        '    REM  ---- Convert Wanted Action 2 -----

        '    Select Case Cells(currentRow, "J").Value
        '        Case 4294967295.0#
        '            Cells(currentRow, "J").Value = "Invalid"
        '        Case 1
        '            Cells(currentRow, "J").Value = "AC_MOVE - Quarantine"
        '        Case 2
        '            Cells(currentRow, "J").Value = "AC_RENAME - Rename"
        '        Case 3
        '            Cells(currentRow, "J").Value = "AC_DEL - Delete"
        '        Case 4
        '            Cells(currentRow, "J").Value = "AC_NOTHING - Leave Alone"
        '        Case 5
        '            Cells(currentRow, "J").Value = "AC_CLEAN - Clean"
        '        Case 6
        '            Cells(currentRow, "J").Value = "AC_REMOVE_MACROS - Remove Macros"
        '        Case 7
        '            Cells(currentRow, "J").Value = "AC_SAVE_AS - Save file as..."
        '        Case 8
        '            Cells(currentRow, "J").Value = "AC_SEND_TO_INTEL - Sent to backend"
        '        Case 9
        '            Cells(currentRow, "J").Value = "AC_MOVE_BACK - Restore from Quarantine"
        '        Case 10
        '            Cells(currentRow, "J").Value = "AC_RENAME_BACK - Rename Back (unused)"
        '        Case 11
        '            Cells(currentRow, "J").Value = "AC_UNDO - Undo Action"
        '        Case 12
        '            Cells(currentRow, "J").Value = "AC_BAD - Error"
        '        Case 13
        '            Cells(currentRow, "J").Value = "AC_BACKUP - Backup to quarantine (backup view)"
        '        Case 14
        '            Cells(currentRow, "J").Value = "AC_PENDING - Pending Analysis"
        '        Case 15
        '            Cells(currentRow, "J").Value = "AC_PARTIAL - Partially Fixed"
        '        Case 16
        '            Cells(currentRow, "J").Value = "AC_TERMINATE - Terminate Process Required"
        '        Case 17
        '            Cells(currentRow, "J").Value = "AC_EXCLUDE - Exclude from Scanning"
        '        Case 18
        '            Cells(currentRow, "J").Value = "AC_REBOOT_PROCESSING - Reboot Processing"
        '        Case 19
        '            Cells(currentRow, "J").Value = "AC_CLEANBYDELETE - Clean by Deletion"
        '        Case 20
        '            Cells(currentRow, "J").Value = "AC_ACCESSDENIED - Access Denied"
        '        Case 21
        '            Cells(currentRow, "J").Value = "AC_TERMINATE_PROCESS_ONLY"
        '        Case 22
        '            Cells(currentRow, "J").Value = "AC_NO_REPAIR"
        '        Case 23
        '            Cells(currentRow, "J").Value = "AC_FAIL"
        '        Case 24
        '            Cells(currentRow, "J").Value = "AC_RUN_POWERTOOL"
        '        Case 25
        '            Cells(currentRow, "J").Value = "AC_NO_REPAIR_POWERTOOL"
        '        Case 110
        '            Cells(currentRow, "J").Value = "AC_INTERESTING_PROCESS_CAL"
        '        Case 111
        '            Cells(currentRow, "J").Value = "AC_INTERESTING_PROCESS_DETECTED"
        '        Case 1000
        '            Cells(currentRow, "J").Value = "AC_INTERESTING_PROCESS_HASHED_DETECTED"
        '        Case 1001
        '            Cells(currentRow, "J").Value = "AC_DNS_HOST_FILE_EXCEPTION"
        '    End Select

        '    REM  ---- Convert Real Action -----

        '    Select Case Cells(currentRow, "K").Value
        '        Case 4294967295.0#
        '            Cells(currentRow, "K").Value = "Invalid"
        '        Case 1
        '            Cells(currentRow, "K").Value = "AC_MOVE - Quarantine"
        '        Case 2
        '            Cells(currentRow, "K").Value = "AC_RENAME - Rename"
        '        Case 3
        '            Cells(currentRow, "K").Value = "AC_DEL - Delete"
        '        Case 4
        '            Cells(currentRow, "K").Value = "AC_NOTHING - Leave Alone"
        '        Case 5
        '            Cells(currentRow, "K").Value = "AC_CLEAN - Clean"
        '        Case 6
        '            Cells(currentRow, "K").Value = "AC_REMOVE_MACROS - Remove Macros"
        '        Case 7
        '            Cells(currentRow, "K").Value = "AC_SAVE_AS - Save file as..."
        '        Case 8
        '            Cells(currentRow, "K").Value = "AC_SEND_TO_INTEL - Sent to backend"
        '        Case 9
        '            Cells(currentRow, "K").Value = "AC_MOVE_BACK - Restore from Quarantine"
        '        Case 10
        '            Cells(currentRow, "K").Value = "AC_RENAME_BACK - Rename Back (unused)"
        '        Case 11
        '            Cells(currentRow, "K").Value = "AC_UNDO - Undo Action"
        '        Case 12
        '            Cells(currentRow, "K").Value = "AC_BAD - Error"
        '        Case 13
        '            Cells(currentRow, "K").Value = "AC_BACKUP - Backup to quarantine (backup view)"
        '        Case 14
        '            Cells(currentRow, "K").Value = "AC_PENDING - Pending Analysis"
        '        Case 15
        '            Cells(currentRow, "K").Value = "AC_PARTIAL - Partially Fixed"
        '        Case 16
        '            Cells(currentRow, "K").Value = "AC_TERMINATE - Terminate Process Required"
        '        Case 17
        '            Cells(currentRow, "K").Value = "AC_EXCLUDE - Exclude from Scanning"
        '        Case 18
        '            Cells(currentRow, "K").Value = "AC_REBOOT_PROCESSING - Reboot Processing"
        '        Case 19
        '            Cells(currentRow, "K").Value = "AC_CLEANBYDELETE - Clean by Deletion"
        '        Case 20
        '            Cells(currentRow, "K").Value = "AC_ACCESSDENIED - Access Denied"
        '        Case 21
        '            Cells(currentRow, "K").Value = "AC_TERMINATE_PROCESS_ONLY"
        '        Case 22
        '            Cells(currentRow, "K").Value = "AC_NO_REPAIR"
        '        Case 23
        '            Cells(currentRow, "K").Value = "AC_FAIL"
        '        Case 24
        '            Cells(currentRow, "K").Value = "AC_RUN_POWERTOOL"
        '        Case 25
        '            Cells(currentRow, "K").Value = "AC_NO_REPAIR_POWERTOOL"
        '        Case 110
        '            Cells(currentRow, "K").Value = "AC_INTERESTING_PROCESS_CAL"
        '        Case 111
        '            Cells(currentRow, "K").Value = "AC_INTERESTING_PROCESS_DETECTED"
        '        Case 1000
        '            Cells(currentRow, "K").Value = "AC_INTERESTING_PROCESS_HASHED_DETECTED"
        '        Case 1001
        '            Cells(currentRow, "K").Value = "AC_DNS_HOST_FILE_EXCEPTION"

        '    End Select


        '    REM  ---- Convert Virus Type -----

        '    Select Case Cells(currentRow, "L").Value
        '        Case 48
        '            Cells(currentRow, "L").Value = "Heuristic"
        '        Case 80
        '            Cells(currentRow, "L").Value = "Hack Tools"
        '        Case 96
        '            Cells(currentRow, "L").Value = "Spyware"
        '        Case 112
        '            Cells(currentRow, "L").Value = "Trackware"
        '        Case 128
        '            Cells(currentRow, "L").Value = "Dialers"
        '        Case 144
        '            Cells(currentRow, "L").Value = "Remote Access"
        '        Case 160
        '            Cells(currentRow, "L").Value = "Adware"
        '        Case 176
        '            Cells(currentRow, "L").Value = "Joke Programs"
        '        Case 224
        '            Cells(currentRow, "L").Value = "Heuristic Application"

        '    End Select

        '    REM  ---- Decode Flags ----
        '    FlagVal = Cells(currentRow, "M").Value
        '    FlagStr = ""

        '    If FlagVal And &H400000 Then
        '        FlagStr = FlagStr + "EB_ACCESS_DENIED "
        '    End If

        '    If FlagVal And &H800000 Then
        '        FlagStr = FlagStr + "EB_NO_VDIALOG "
        '    End If

        '    If FlagVal And &H1000000 Then
        '        FlagStr = FlagStr + "EB_LOG "
        '    End If

        '    If FlagVal And &H2000000 Then
        '        FlagStr = FlagStr + "EB_REAL_CLIENT "
        '    End If

        '    If FlagVal And &H4000000 Then
        '        FlagStr = FlagStr + "EB_ENDUSER_BLOCKED "
        '    End If

        '    If FlagVal And &H8000000 Then
        '        FlagStr = FlagStr + "EB_AP_FILE_WIPED "
        '    End If

        '    If FlagVal And &H10000000 Then
        '        FlagStr = FlagStr + "EB_PROCESS_KILLED "
        '    End If

        '    If FlagVal And &H20000000 Then
        '        FlagStr = FlagStr + "EB_FROM_CLIENT "
        '    End If

        '    If FlagVal And &H40000000 Then
        '        FlagStr = FlagStr + "EB_EXTRN_EVENT "
        '    End If

        '    If FlagVal And &H1FF Then

        '        If FlagVal And &H1 Then
        '            FlagStr = FlagStr + "FA_SCANNING_MEMORY "
        '        End If

        '        If FlagVal And &H2 Then
        '            FlagStr = FlagStr + "FA_SCANNING_BOOT_SECTOR "
        '        End If

        '        If FlagVal And &H4 Then
        '            FlagStr = FlagStr + "FA_SCANNING_FILE "
        '        End If

        '        If FlagVal And &H8 Then
        '            FlagStr = FlagStr + "FA_SCANNING_BEHAVIOR "
        '        End If

        '        If FlagVal And &H10 Then
        '            FlagStr = FlagStr + "FA_SCANNING_CHECKSUM "
        '        End If

        '        If FlagVal And &H20 Then
        '            FlagStr = FlagStr + "FA_WALKSCAN "
        '        End If

        '        If FlagVal And &H40 Then
        '            FlagStr = FlagStr + "FA_RTSSCAN "
        '        End If

        '        If FlagVal And &H80 Then
        '            FlagStr = FlagStr + "FA_CHECK_SCAN "
        '        End If

        '        If FlagVal And &H100 Then
        '            FlagStr = FlagStr + "FA_CLEAN_SCAN "
        '        End If

        '    End If

        '    If FlagVal And &H803FFE00 Then
        '        FlagStr = FlagStr + "EB_N_OVERLAYS ("

        '        If FlagVal And &H200 Then
        '            FlagStr = FlagStr + "N_OFFLINE "
        '        End If

        '        If FlagVal And &H400 Then
        '            FlagStr = FlagStr + "N_INFECTED "
        '        End If

        '        If FlagVal And &H800 Then
        '            FlagStr = FlagStr + "N_REPSEED_SCAN "
        '        End If

        '        If FlagVal And &H1000 Then
        '            FlagStr = FlagStr + "N_RTSNODE "
        '        End If

        '        If FlagVal And &H2000 Then
        '            FlagStr = FlagStr + "N_MAILNODE "
        '        End If

        '        If FlagVal And &H4000 Then
        '            FlagStr = FlagStr + "N_FILENODE "
        '        End If

        '        If FlagVal And &H8000 Then
        '            FlagStr = FlagStr + "N_COMPRESSED "
        '        End If

        '        If FlagVal And &H10000 Then
        '            FlagStr = FlagStr + "N_PASSTHROUGH "
        '        End If

        '        If FlagVal And &H40000 Then
        '            FlagStr = FlagStr + "N_DIRNODE "
        '        End If

        '        If FlagVal And &H80000 Then
        '            FlagStr = FlagStr + "N_ENDNODE "
        '        End If

        '        If FlagVal And &H100000 Then
        '            FlagStr = FlagStr + "N_MEMNODE "
        '        End If

        '        If FlagVal And &H200000 Then
        '            FlagStr = FlagStr + "N_ADMIN_REQUEST_REMEDIATION "
        '        End If

        '        FlagStr = RTrim(FlagStr) + ")"

        '    End If

        '    Cells(currentRow, "M").Value = RTrim(FlagStr)

        '    REM  ---- Convert Eraser Status -----

        '    Select Case Cells(currentRow, "BC").Value
        '        Case 0
        '            Cells(currentRow, "BC").Value = "Success"
        '        Case 1
        '            Cells(currentRow, "BC").Value = "Reboot Required"
        '        Case 2
        '            Cells(currentRow, "BC").Value = "Nothing To Do"
        '        Case 3
        '            Cells(currentRow, "BC").Value = "Repaired"
        '        Case 4
        '            Cells(currentRow, "BC").Value = "Deleted"
        '        Case 5
        '            Cells(currentRow, "BC").Value = "False"
        '        Case 6
        '            Cells(currentRow, "BC").Value = "Abort"
        '        Case 7
        '            Cells(currentRow, "BC").Value = "Continue"
        '        Case 8
        '            Cells(currentRow, "BC").Value = "Service Not Stopped"
        '        Case 9
        '            Cells(currentRow, "BC").Value = "Application Heuristic Scan Failure"
        '        Case 10
        '            Cells(currentRow, "BC").Value = "Cannot Remediate"
        '        Case 11
        '            Cells(currentRow, "BC").Value = "Whitelist Failure"
        '        Case 12
        '            Cells(currentRow, "BC").Value = "Driver Failure"
        '        Case 13
        '            Cells(currentRow, "BC").Value = "Reserved01"
        '        Case 13
        '            Cells(currentRow, "BC").Value = "Commercial Application List Failure"
        '        Case 13
        '            Cells(currentRow, "BC").Value = "Application Heuristic Scan Invalid OS"
        '        Case 13
        '            Cells(currentRow, "BC").Value = "Content Manager Data Error"
        '        Case 999
        '            Cells(currentRow, "BC").Value = "Leave Alone"
        '        Case 1000
        '            Cells(currentRow, "BC").Value = "Generic Failure"
        '        Case 1001
        '            Cells(currentRow, "BC").Value = "Out Of Memory"
        '        Case 1002
        '            Cells(currentRow, "BC").Value = "Not Initialized"
        '        Case 1003
        '            Cells(currentRow, "BC").Value = "Invalid Argument"
        '        Case 1004
        '            Cells(currentRow, "BC").Value = "Insufficient Buffer"
        '        Case 1005
        '            Cells(currentRow, "BC").Value = "Decryption Error"
        '        Case 1006
        '            Cells(currentRow, "BC").Value = "File Not Found"
        '        Case 1007
        '            Cells(currentRow, "BC").Value = "Out Of Range"
        '        Case 1008
        '            Cells(currentRow, "BC").Value = "COM Error"
        '        Case 1009
        '            Cells(currentRow, "BC").Value = "Partial Failure"
        '        Case 1010
        '            Cells(currentRow, "BC").Value = "Bad Definitions"
        '        Case 1011
        '            Cells(currentRow, "BC").Value = "Invalid Command"
        '        Case 1012
        '            Cells(currentRow, "BC").Value = "No Interface"
        '        Case 1013
        '            Cells(currentRow, "BC").Value = "RSA Error"
        '        Case 1014
        '            Cells(currentRow, "BC").Value = "Path Not Empty"
        '        Case 1015
        '            Cells(currentRow, "BC").Value = "Invalid Path"
        '        Case 1016
        '            Cells(currentRow, "BC").Value = "Path Not Empty"
        '        Case 1017
        '            Cells(currentRow, "BC").Value = "File Still Present"
        '        Case 1018
        '            Cells(currentRow, "BC").Value = "Invalid OS"
        '        Case 1019
        '            Cells(currentRow, "BC").Value = "Not Implemented"
        '        Case 1020
        '            Cells(currentRow, "BC").Value = "Access Denied"
        '        Case 1021
        '            Cells(currentRow, "BC").Value = "Directory Still Present"
        '        Case 1022
        '            Cells(currentRow, "BC").Value = "Inconsistent State"
        '        Case 1023
        '            Cells(currentRow, "BC").Value = "Timeout"
        '        Case 1024
        '            Cells(currentRow, "BC").Value = "Action Pending"
        '        Case 1025
        '            Cells(currentRow, "BC").Value = "Volume Write Protected"
        '        Case 1026
        '            Cells(currentRow, "BC").Value = "Not Reparse Point"
        '        Case 1027
        '            Cells(currentRow, "BC").Value = "File Exists"
        '        Case 1028
        '            Cells(currentRow, "BC").Value = "Target Protected"
        '        Case 1029
        '            Cells(currentRow, "BC").Value = "Disk Full"
        '        Case 1030
        '            Cells(currentRow, "BC").Value = "Shutdown In Progress"
        '        Case 1031
        '            Cells(currentRow, "BC").Value = "Media Error"
        '        Case 1032
        '            Cells(currentRow, "BC").Value = "Network Defs Error"

        '    End Select


        '    REM  ---- Convert Eraser Category ID -----

        '    Select Case Cells(currentRow, "BI").Value
        '        Case 1
        '            Cells(currentRow, "BI").Value = "ECat_HeuristicTrojanWorm"
        '        Case 2
        '            Cells(currentRow, "BI").Value = "ECat_HeuristicKeyLogger"
        '        Case 100
        '            Cells(currentRow, "BI").Value = "ECat_CommercialRemoteControl"
        '        Case 101
        '            Cells(currentRow, "BI").Value = "ECat_CommercialKeyLogger"
        '        Case 200
        '            Cells(currentRow, "BI").Value = "ECat_Cookie"
        '        Case 300
        '            Cells(currentRow, "BI").Value = "ECat_Shields"

        '    End Select

        '    REM  ---- Convert Dynamic Categoryset ID -----

        '    Select Case Cells(currentRow, "BJ").Value
        '        Case 1
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_MALWARE"
        '        Case 2
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_SECURITY_RISK"
        '        Case 3
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_POTENTIALLY_UNWANTED_APPLICATIONS"
        '        Case 4
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_EXPERIMENTAL_HEURISTIC"
        '        Case 5
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_LEGACY_VIRAL"
        '        Case 6
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_LEGACY_NON_VIRAL"
        '        Case 7
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_CRIMEWARE"
        '        Case 8
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_ADVANCED_HEURISTICS"
        '        Case 9
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_SET_REPUTATION_BACKED_ADVANCED_HEURISTICS"
        '        Case 10
        '            Cells(currentRow, "BJ").Value = "SCANW_CATEGORY_PREVALENCE_BACKED_ADVANCED_HEURISTICS"

        '    End Select

        '    REM  ---- Convert Display Name To Use -----

        '    Select Case Cells(currentRow, "BL").Value
        '        Case 0
        '            Cells(currentRow, "BL").Value = "Application Name"
        '        Case 1
        '            Cells(currentRow, "BL").Value = "VID Virus Name"

        '    End Select

        '    REM  ---- Convert Reputation Disposition -----

        '    Select Case Cells(currentRow, "BM").Value
        '        Case 0
        '            Cells(currentRow, "BM").Value = "Good"
        '        Case 1
        '            Cells(currentRow, "BM").Value = "Bad"
        '        Case 127
        '            Cells(currentRow, "BM").Value = "Unknown"

        '    End Select

        '    REM  ---- Convert Detection Type -----

        '    Select Case Cells(currentRow, "BU").Value
        '        Case 0
        '            Cells(currentRow, "BU").Value = "Traditional"
        '        Case 1
        '            Cells(currentRow, "BU").Value = "Heuristic"

        '    End Select

        '    REM  ---- Convert VSIC State -----

        '    Select Case Cells(currentRow, "BW").Value
        '        Case 0
        '            Cells(currentRow, "BW").Value = "Off"
        '        Case 1
        '            Cells(currentRow, "BW").Value = "On"
        '        Case 2
        '            Cells(currentRow, "BW").Value = "Failed"

        '    End Select

        '    REM  ---- Scan Duration -----

        '    TimeString = Cells(currentRow, "BY").Value

        '    SecondVal = CInt(TimeString)

        '    If SecondVal > 0 Then
        '        Cells(currentRow, "BY").Value = TimeSerial(0, 0, SecondVal)
        '        Cells(currentRow, "BY").NumberFormat = "hh:mm:ss"
        '    End If

        '    REM  ---- Convert Scan Start Time -----

        '    TimeString = Cells(currentRow, "BZ").Value

        '    REM ---- Added an IF condition to only proceed with conversion if BZ cell is not empty ----
        '    If TimeString <> "" Then
        '        YearVal = CInt("&H" & Mid(TimeString, 1, 2)) + 1970
        '        MonthVal = CInt("&H" & Mid(TimeString, 3, 2)) + 1
        '        DayVal = CInt("&H" & Mid(TimeString, 5, 2))
        '        HourVal = CInt("&H" & Mid(TimeString, 7, 2))
        '        MinuteVal = CInt("&H" & Mid(TimeString, 9, 2))
        '        SecondVal = CInt("&H" & Mid(TimeString, 11, 2))

        '        Cells(currentRow, "BZ").Value = DateSerial(YearVal, MonthVal, DayVal) + TimeSerial(HourVal, MinuteVal, SecondVal)
        '        Cells(currentRow, "BZ").NumberFormat = "mm/dd/yyyy hh:mm:ss"
        '    End If

        '    REM  ---- Convert TargetApp Type -----

        '    Select Case Cells(currentRow, "CA").Value
        '        Case 0
        '            Cells(currentRow, "CA").Value = "Normal"
        '        Case 1
        '            Cells(currentRow, "CA").Value = "Modern (Metro)"

        '    End Select

        'Next

        'REM  ---- Resize all cells -----

        'Cells.Select()
        'Selection.Columns.AutoFit()

        'Application.ScreenUpdating = True

        'REM  ---- Select the first cell containing data and freeze the top-most row and left-most column -----

        'Range("B2").Select()

    End Sub

#End Region

    Public Function PerformVirusScan(ByVal strFileName As String, ByVal strAntivirusExePath As String) As Boolean

        Try
            If Not File.Exists(My.Settings.AntivirusExePath.Trim) Then
                Return False
            End If

            Dim myProcArg As String = " /ScanFile " & """" & strFileName & """"

            Dim ProcStartInfo As ProcessStartInfo = New ProcessStartInfo()
            'Do Not Show CMD Window
            ProcStartInfo.CreateNoWindow = True

            ProcStartInfo.FileName = strAntivirusExePath
            ProcStartInfo.Arguments = myProcArg
            ProcStartInfo.RedirectStandardInput = True
            ProcStartInfo.RedirectStandardOutput = True
            ProcStartInfo.RedirectStandardError = True
            ProcStartInfo.UseShellExecute = False

            Dim ProcessScan As Process = New Process()
            ProcessScan.StartInfo = ProcStartInfo
            ProcessScan.EnableRaisingEvents = True

            'Start Scan!
            ProcessScan.Start()

            Dim inputWriter As StreamWriter = ProcessScan.StandardInput
            Dim outputReader As StreamReader = ProcessScan.StandardOutput
            Dim errorReader As StreamReader = ProcessScan.StandardError

            ProcessScan.WaitForExit()
            ProcessScan.Close()

            'Clear Memory
            ProcessScan.Dispose()
            ProcessScan = Nothing

            inputWriter.Dispose()
            outputReader.Dispose()
            errorReader.Dispose()

            inputWriter = Nothing
            outputReader = Nothing
            errorReader = Nothing



            ''Do AV check here
            'Dim myProcess As New Process()

            ''Address of command line virus scan exe
            'myProcess.StartInfo.FileName = strAntivirusExePath

            'Dim myProcArg As String = " /ScanFile " & """" & strFileName & """"
            'myProcess.StartInfo.Arguments = myProcArg
            'myProcess.StartInfo.UseShellExecute = False
            'myProcess.StartInfo.RedirectStandardInput = True
            'myProcess.StartInfo.RedirectStandardOutput = True
            'myProcess.StartInfo.RedirectStandardError = True
            'myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden

            'myProcess.Start()

            'Dim inputWriter As StreamWriter = myProcess.StandardInput
            'Dim outputReader As StreamReader = myProcess.StandardOutput
            'Dim errorReader As StreamReader = myProcess.StandardError

            'myProcess.WaitForExit()
            'myProcess.Close()

            'inputWriter = Nothing
            'outputReader = Nothing
            'errorReader = Nothing

            Return True

        Catch Ex As Exception
            Throw Ex
        End Try

    End Function

    Public Function IsVirusFound(ByVal strFileName As String, ByVal strAntivirusLogPath As String) As String


        'Log Line Content:
        '   Time
        '   Event
        '   Category
        '   Logger
        '   Computer
        '   User
        '   Virus
        '   File
        '   Wanted Action 1
        '   Wanted Action 2
        '   Real Action
        '   Virus Type
        '   Flags
        '   Description
        '   ScanID
        '   New_Ext
        '   Group ID
        '   Event Data
        '   VBin_ID
        '   Virus ID
        '   Quarantine Forward Status
        '   Access
        '   SND_Status
        '   Compressed
        '   Depth
        '   Still Infected
        '   Def Info
        '   Def Sequence Number
        '   Clean Info
        '   Delete Info
        '   Backup ID
        '   Parent
        '   GUID
        '   Client Group
        '   Address
        '   Domain Name
        '   NT Domain
        '   MAC Address
        '   Version
        '   Remote Machine
        '   Remote Machine IP
        '   Action 1 Status
        '   Action 2 Status
        '   License Feature Name
        '   License Feature Version
        '   License Serial Number
        '   License Fulfillment ID
        '   License Start Date
        '   License Expiration Date
        '   License LifeCycle
        '   License Seats Total
        '   License Seats
        '   Error Code
        '   License Seats Delta
        '   Status
        '   Domain GUID
        '   Log Session GUID
        '   VBin Session ID
        '   Login Domain
        '   Event Data 2
        '   Eraser Category ID
        '   Dynamic Categoryset ID
        '   Dynamic Subcategoryset ID
        '   Display Name To Use
        '   Reputation Disposition
        '   Reputation Confidence
        '   First Seen
        '   Reputation Prevalence
        '   Downloaded URL
        '   Creator For Dropper
        '   CIDS State
        '   Behavior Risk Level
        '   Detection Type
        '   Acknowledge Text
        '   VSIC State
        '   Scan GUID
        '   Scan Duration
        '   Scan Start Time
        '   TargetApp Type
        '   Scan Command GUID


        Try
            IsVirusFound = ""

            'Debug.Print(CStr(Date.Now) & " Start Processing Log")

            Dim arrLogLine As String()

            Dim dtVirusScanDate As Date = DateAdd(DateInterval.Second, -5, Date.Now)

            Dim strLogFileName As String = Date.Now.ToString("MMddyyyy") & ".Log" '05112017.Log

            'For Debug Only!
            'strLogFileName = "05112017.Log"

            If Directory.Exists(strAntivirusLogPath) Then

                'Check if Log Folder Exist
                If Not strAntivirusLogPath.Trim.EndsWith("\") Then
                    strAntivirusLogPath = strAntivirusLogPath.Trim & "\"
                End If

                'Check If Log File Exist
                If File.Exists(strAntivirusLogPath & strLogFileName) Then

                    Dim fs As New FileStream(strAntivirusLogPath & strLogFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                    Dim stmReader As New StreamReader(fs)
                    Do Until (stmReader.EndOfStream)
                        Dim _Line As String = stmReader.ReadLine.Trim
                        arrLogLine = _Line.Split(CChar(","))
                        'Chek Log Date & Time
                        If GetLogDateTime(arrLogLine(0)) >= DateAdd(DateInterval.Second, -10, Date.Now) And GetLogDateTime(arrLogLine(0)) <= Date.Now Then

                            'Debug.Print(CStr(GetLogDateTime(arrLogLine(0)) & " " & arrLogLine(13).Trim))

                            'Check Virus Name
                            If arrLogLine(6).Trim <> "" Then
                                'Check File Name
                                If arrLogLine(7).Trim.IndexOf(strFileName) > 0 Then
                                    'Some Virus Found!
                                    IsVirusFound = arrLogLine(6).Trim
                                    Exit Do
                                End If
                            End If

                        End If
                    Loop

                    stmReader.Dispose()
                    stmReader = Nothing

                End If
            End If

            'Debug.Print(CStr(Date.Now) & " Finish Processing Log")

            Return IsVirusFound

        Catch Ex As Exception
            Throw Ex
        End Try

    End Function

    Private Function GetLogDateTime(ByVal sreValue As String) As Date

        Try
            Dim _Year As Integer
            Dim _Month As Integer
            Dim _Day As Integer
            Dim _Hour As Integer
            Dim _Minute As Integer
            Dim _Second As Integer

            Dim currDate As Date

            _Year = CInt("&H" & Mid(sreValue, 1, 2)) + 1970
            _Month = CInt("&H" & Mid(sreValue, 3, 2)) + 1
            _Day = CInt("&H" & Mid(sreValue, 5, 2))
            _Hour = CInt("&H" & Mid(sreValue, 7, 2))
            _Minute = CInt("&H" & Mid(sreValue, 9, 2))
            _Second = CInt("&H" & Mid(sreValue, 11, 2))

            currDate = New Date(_Year, _Month, _Day, _Hour, _Minute, _Second)
            Return currDate

        Catch Ex As Exception
            Throw Ex
        End Try

    End Function


End Module
