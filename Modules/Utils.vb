﻿Option Explicit On
Option Strict On

Imports System.IO
Imports System.Runtime.CompilerServices
Imports System.Reflection
Imports System.Reflection.MethodBase
Imports System.Net
Imports System.ComponentModel
Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf


Module Utils

    Public Function GetModifiedFileDate(ByVal strFileName As String) As String
        Try
            GetModifiedFileDate = File.GetLastWriteTime(strFileName).ToString

        Catch Ex As FileNotFoundException
            GetModifiedFileDate = "Unknown"

        Catch Ex As Exception
            GetModifiedFileDate = "  /  /  "
        End Try
    End Function

    'Base Conversion Functions
    Public Function ToBase64(ByVal data() As Byte) As String
        If data Is Nothing Then Throw New ArgumentNullException("data")
        Return Convert.ToBase64String(data)
    End Function

    Public Function FromBase64(ByVal base64 As String) As Byte()
        If base64 Is Nothing Then Throw New ArgumentNullException("base64")
        Return Convert.FromBase64String(base64)
    End Function

    Public Function ByteArrayToFile(ByVal arrData() As Byte, ByVal strFilePath As String) As Boolean

        Try
            'Swith To Admin User
            Call ImpersonateAdmin()

            'Open File
            Dim fStream As New FileStream(strFilePath, FileMode.OpenOrCreate, FileAccess.Write)

            'Write array to file 
            fStream.Write(arrData, 0, arrData.Length)

            'Destroy Objects
            fStream.Close()
            fStream.Dispose()

            'Swith Back To Original User
            Call UndoImpersonation()

            Return True

        Catch Ex As Exception
            Call DebugOutput("General Exception!" & vbNewLine & GetCurrentMethod.Name & vbNewLine & Ex.Message, DebugMode.Normal)
            Return False
        End Try

    End Function

    Public Function GetResponseTime(ByVal mSessionInfo As SessionInfo) As Double

        Try
            Dim responseTime As Long = 0
            responseTime = Date.Now.Ticks - CLng(mSessionInfo.TimeStamp)
            GetResponseTime = (responseTime / 10 ^ 7)

        Catch Ex As Exception
            'TODO:
            Return -0.01
        End Try

    End Function

    Public Function IsValidPDF(ByVal strPdfFileName As String) As Boolean

        Try
            Dim myPdfReader As New PdfReader(strPdfFileName)
            myPdfReader.Dispose()
            myPdfReader = Nothing
            Return True

        Catch generatedExceptionName As iTextSharp.text.exceptions.InvalidPdfException
            Call DebugOutput("iTextSharp Exception!" & vbNewLine & generatedExceptionName.Message, DebugMode.Normal)
            Return False

        Catch Ex As Exception
            Call DebugOutput("General Exception!" & vbNewLine & GetCurrentMethod.Name & vbNewLine & Ex.Message, DebugMode.Normal)
            Return False
        End Try

    End Function

    Public Function GetImageFormat(ByVal arrData As Byte()) As DocumentType

        Try
            'http://www.mikekunz.com/image_file_header.html

            'BMP Header
            Dim bmp = Encoding.ASCII.GetBytes("BM")

            'GIF Header
            Dim gif = Encoding.ASCII.GetBytes("GIF")

            'PNG Header
            Dim png = New Byte() {137, 80, 78, 71}

            'TIF Header
            Dim tif = New Byte() {73, 73, 42}

            'TIFF Header
            Dim tiff = New Byte() {77, 77, 42}

            'JPG Header
            Dim jpg = New Byte() {255, 216, 255, 224}

            'JPEG Canon Header
            Dim jpeg = New Byte() {255, 216, 255, 225}


            If bmp.SequenceEqual(arrData.Take(bmp.Length)) Then
                Return DocumentType.BMP
            End If

            If gif.SequenceEqual(arrData.Take(gif.Length)) Then
                Return DocumentType.GIF
            End If

            If png.SequenceEqual(arrData.Take(png.Length)) Then
                Return DocumentType.PNG
            End If

            If tif.SequenceEqual(arrData.Take(tif.Length)) Then
                Return DocumentType.TIF
            End If

            If tiff.SequenceEqual(arrData.Take(tiff.Length)) Then
                Return DocumentType.TIF
            End If

            If jpg.SequenceEqual(arrData.Take(jpg.Length)) Then
                Return DocumentType.JPG
            End If

            If jpeg.SequenceEqual(arrData.Take(jpeg.Length)) Then
                Return DocumentType.JPG
            End If

            Return DocumentType.UNKNOWN

        Catch Ex As Exception
            Call DebugOutput("General Exception!" & vbNewLine & GetCurrentMethod.Name & vbNewLine & Ex.Message, DebugMode.Normal)
            Return DocumentType.UNKNOWN
        End Try

    End Function

    Public Function IsValidImage(ByVal strImageFilePath As String, ByVal ImageFileType As DocumentType) As Boolean
        Try

            IsValidImage = False

            Dim imgObj As System.Drawing.Image = System.Drawing.Image.FromFile(strImageFilePath)

            Select Case ImageFileType
                'BMP
                Case Is = DocumentType.BMP
                    If (imgObj.RawFormat.Guid = Imaging.ImageFormat.Bmp.Guid) Then
                        IsValidImage = True
                    End If

                    'GIF
                Case Is = DocumentType.GIF
                    If (imgObj.RawFormat.Guid = Imaging.ImageFormat.Gif.Guid) Then
                        IsValidImage = True
                    End If

                    'JPG
                Case Is = DocumentType.JPG
                    If (imgObj.RawFormat.Guid = Imaging.ImageFormat.Jpeg.Guid) Then
                        IsValidImage = True
                    End If

                    'PNG
                Case Is = DocumentType.PNG
                    If (imgObj.RawFormat.Guid = Imaging.ImageFormat.Png.Guid) Then
                        IsValidImage = True
                    End If

                    'TIF
                Case Is = DocumentType.TIF
                    If (imgObj.RawFormat.Guid = Imaging.ImageFormat.Tiff.Guid) Then
                        IsValidImage = True
                    End If

                    'UNKNOWN Format
                Case Else
                    IsValidImage = False
            End Select

            imgObj.Dispose()
            imgObj = Nothing


        Catch Ex As Exception
            Call DebugOutput("General Exception!" & vbNewLine & GetCurrentMethod.Name & vbNewLine & Ex.Message, DebugMode.Normal)
            Return False
        End Try

    End Function

    Public Sub CreatePDFFile(ByVal strImgFilePath As String, ByVal strPdfFilePath As String)

        Dim pdfDoc As New Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35)

        Try
            'Create Document class object and set its size to letter and give space left, right, Top, Bottom Margin
            Dim pdfWriter As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(strPdfFilePath, FileMode.Create))

            'Open Document to write
            pdfDoc.Open()

            'Write some content into pdf file
            Dim paragraph As New Paragraph("This is my first line using Paragraph.")

            'Now image in the pdf file
            Dim img As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(strImgFilePath)

            'Resize image depend upon your need
            img.ScaleToFit(280.0F, 260.0F)

            'Give space before image
            img.SpacingBefore = 30.0F

            'Give some space after the image
            img.SpacingAfter = 1.0F
            img.Alignment = Element.ALIGN_CENTER

            'add paragraph to the document
            pdfDoc.Add(paragraph)

            'add an image to the created pdf document
            pdfDoc.Add(img)

            'handle pdf document exception if any
        Catch docEx As DocumentException
            'handle IO exception

        Catch ioEx As IOException
            'ahndle other exception if occurs

        Catch Ex As Exception

        Finally
            'Close document and writer
            pdfDoc.Close()
            pdfDoc.Dispose()
            pdfDoc = Nothing

        End Try

    End Sub

    Public Function IsValidIP(ByVal sIpAddress As String) As Boolean

        If Not Regex.IsMatch(sIpAddress, "\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b") Then
            Return False
        End If

        Dim dummy As IPAddress = Nothing
        Return IPAddress.TryParse(sIpAddress, dummy)

    End Function

    Public Function GetEnumDescription(ByVal EnumConstant As [Enum]) As String

        'This procedure gets the <Description> attribute of an enum constant, if any.
        'Otherwise it gets the string name of the enum member.
        Try
            Dim _FieldInfo As FieldInfo = EnumConstant.GetType().GetField(EnumConstant.ToString())
            Dim arrAttribute() As DescriptionAttribute = DirectCast(_FieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())
            If arrAttribute.Length > 0 Then
                Return arrAttribute(0).Description
            Else
                Return EnumConstant.ToString
            End If
        Catch Ex As Exception
            Return ""
        End Try

    End Function

    Public Function CheckErrorFolderPath() As String

        Try
            'Build PDF Error Folder Path
            Dim strErrorPath As String = ""
            If Not My.Settings.LogPath.EndsWith("\") Then
                strErrorPath = My.Settings.LogPath & "\"
            End If
            strErrorPath = strErrorPath & "Error"

            'Check For Error Folder
            If Not My.Computer.FileSystem.DirectoryExists(strErrorPath) Then
                My.Computer.FileSystem.CreateDirectory(strErrorPath)
            End If

            Return strErrorPath

        Catch Ex As Exception
            Call DebugOutput("General Exception!" & vbNewLine & GetCurrentMethod.Name & vbNewLine & Ex.Message, DebugMode.Normal)
            If GetErrorCode(Ex) = ErrorCode.UnknownError Then
                'Set General WS Error Message
                Throw New WSException(Ex.Message, Ex)
            Else
                Throw Ex
            End If
        End Try

    End Function

    Public Sub MoveToErrorFolder(ByVal strSourceFilePath As String, ByVal strDestinationFilePath As String)

        Try

            'Set the File(s)Info 
            Dim myLocaPdfFile As FileInfo
            Dim myErrorPdfFile As FileInfo

            'Copy from SourceFile Path to the DestinationFile Path
            myLocaPdfFile = New FileInfo(fileName:=strSourceFilePath)
            myErrorPdfFile = myLocaPdfFile.CopyTo(destFileName:=strDestinationFilePath, overwrite:=True)

            'Delete SourceFile File
            If myErrorPdfFile.Exists = True Then
                myLocaPdfFile.Delete()
            End If

            myLocaPdfFile = Nothing
            myErrorPdfFile = Nothing

        Catch Ex As Exception
            Call DebugOutput("General Exception!" & vbNewLine & GetCurrentMethod.Name & vbNewLine & Ex.Message, DebugMode.Normal)
            If GetErrorCode(Ex) = ErrorCode.UnknownError Then
                'Set General WS Error Message
                Throw New WSException(Ex.Message, Ex)
            Else
                Throw Ex
            End If
        End Try

    End Sub

End Module

'***************************************
'*** HttpContextExtensions Extension ***
'***************************************
Module HttpContextExtensions
    <Extension()>
    Public Function IP(ByVal context As System.Web.HttpContext) As String
        Return context.Request.ServerVariables("REMOTE_ADDR").Replace("::1", "127.0.0.1") 'IPv6 localhost
    End Function
End Module










