﻿Option Explicit On
Option Strict On

Imports System.Xml

Module WSExceptions

    Public Class WSException
        Inherits Exception
        Public Sub New(ByVal Message As String)
            MyBase.New(Message)
        End Sub
        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Message As String, ByVal Ex As Exception)
            MyBase.New(Message, Ex)
        End Sub
    End Class

    'For use in the GetCustomet function to differintiate from other General Exceptions
    Public Class InnerCustomerNotFoundException
        Inherits WSException
        Public Sub New(ByVal Message As String)
            MyBase.New(Message)
        End Sub
    End Class

    Public Class InvalidDeviceSNException
        Inherits WSException
        Public Sub New(ByVal DeviceSN As Integer)
            MyBase.New("No such DeviceSN found: " & DeviceSN)
        End Sub
    End Class

    Public Class MissingXMLTagsException
        Inherits WSException
        Public Sub New(ByVal Message As String)
            MyBase.New("Missing/Empty tag: " & Message)
        End Sub
    End Class

    'Return Error Codes
    Enum ErrorCode As Byte
        DBError = 1                     'DB error
        BadFormatXML = 2                'Bad format XML
        MissingTags = 3                 'Missing tags
        UnknownError = 255              'Other/Unknown!
    End Enum

    Public Function GetErrorCode(ByVal Ex As Exception) As ErrorCode

        Select Case True
            Case TypeOf Ex Is SqlClient.SqlException
                Return ErrorCode.DBError                        'DB error
            Case TypeOf Ex Is XmlException
                Return ErrorCode.BadFormatXML                   'Bad format XML
            Case TypeOf Ex Is MissingXMLTagsException
                Return ErrorCode.MissingTags                    'Missing tags
            Case Else
                Return ErrorCode.UnknownError                   'Other/Unknown!
        End Select

    End Function

End Module
