﻿Option Explicit On
Option Strict On

Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO

Module XMLFunctions

    Public Function NormalizeXmlString(ByVal strXmlString As String) As String

        'TODO: For Debug Only!
        'Return strXmlString

        Try
            '**********************************************************
            'Character | Entity Number | Entity Name | Description
            '**********************************************************
            '   "           "            &quot;        quotation mark
            '   '           '            &apos;        apostrophe 
            '   &           &            &amp;         ampersand
            '   <           <            &lt;          less-than
            '   >           >            &gt;          greater-than
            '**********************************************************
            'SecurityElement.Escape(Method)
            'http://msdn.microsoft.com/en-us/library/system.security.securityelement.escape%28VS.80%29.aspx#Y499

            If strXmlString.Trim = "" Then Return ""

            Dim swErrorInXml As Boolean = True
            Dim _XmlString As String = strXmlString.Trim

            'Try to Load XML String
            Dim xDocument As New XmlDocument
            Try
                xDocument.LoadXml(_XmlString)
                swErrorInXml = False
            Catch Ex As Exception
                swErrorInXml = True
            Finally
                xDocument = Nothing
            End Try

            If swErrorInXml = True Then
                'Decode XML String
                _XmlString = HttpUtility.UrlDecode(_XmlString)
                Call DebugOutput("Decoded XML Data is: " & vbNewLine & _XmlString, DebugMode.Normal)

                '_XmlString = _XmlString.Replace("&", "&amp;").Replace("""", "&quot;").Replace("'", "&apos;")

                'Use:
                'Replace "&" with "&amp;"
                '_XmlString = _XmlString.Replace("&", "&amp;")
                'Or:
                'Eencode "&", but not "&amp;", "&gt;", "&lt;", or "&quot;".
                _XmlString = Regex.Replace(_XmlString, "&(?!(?:amp|lt|gt|quot);)", "&amp;", RegexOptions.IgnoreCase)

                _XmlString = CleanInvalidXmlChars(_XmlString)
                Call DebugOutput("Cleaned XML Data is: " & vbNewLine & _XmlString, DebugMode.Normal)
            End If

            Return _XmlString

        Catch Ex As Exception
            Throw Ex
            Return strXmlString
        End Try

    End Function

    Private Function CleanInvalidXmlChars(ByVal strXML As String) As String
        'From XML spec valid chars: 
        '#x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]     
        'any Unicode character, excluding the surrogate blocks, FFFE, and FFFF. 
        Dim re As String = "[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]"
        Return Regex.Replace(strXML, re, "")
    End Function

    Private Sub ExpandEmptyTags(ByRef XD As XmlDocument)
        'Replaces self closing tags with open and close tags. Removes linebreaks.
        'Used by ToXmlDocument
        For Each xElement As XmlElement In XD.SelectNodes("//*[not(node())]")
            xElement.IsEmpty = False
        Next
    End Sub

    'Generic Serialization/Deserialization Functions
    Public Function DeserializeXmlData(Of T)(ByVal strXmlData As String) As T
        Try
            Dim myXmlSerializer As New XmlSerializer(GetType(T))
            Dim myStringReader As New StringReader(strXmlData)
            DeserializeXmlData = CType(myXmlSerializer.Deserialize(myStringReader), T)

            myStringReader.Close()
            myStringReader = Nothing
            myXmlSerializer = Nothing

            GC.Collect()

        Catch Ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SerializeXmlData(Of T)(ByVal AnyType As T) As XmlDocument

        'Get the xml serialization of the given object
        Dim xmlSerializer As New XmlSerializer(AnyType.GetType)
        Dim xmlNamespace As New XmlSerializerNamespaces
        Dim myStringWriter As New StringWriter
        Dim myXmlDocument As New XmlDocument

        Try
            xmlNamespace.Add("", "")

            xmlSerializer.Serialize(myStringWriter, AnyType, xmlNamespace)
            myXmlDocument.PreserveWhitespace = True ' removes CRLF
            myXmlDocument.LoadXml(myStringWriter.ToString)
            Call ExpandEmptyTags(myXmlDocument) ' change <A /> to <A></A>
            Return myXmlDocument

        Catch Ex As Exception
            Return Nothing

        Finally
            xmlSerializer = Nothing
            xmlNamespace = Nothing
            myStringWriter.Dispose()
            myStringWriter = Nothing
            myXmlDocument = Nothing
        End Try

    End Function

    Public Function ToXmlDocument(Of T)(ByVal AnyType As T) As XmlDocument

        'Get the xml serialization of the given object
        Dim xmlSerializer As New XmlSerializer(AnyType.GetType)
        Dim xmlNamespace As New XmlSerializerNamespaces
        Dim myStringWriter As New StringWriter
        Dim myXmlDocument As New XmlDocument

        Try
            xmlNamespace.Add("", "")

            xmlSerializer.Serialize(myStringWriter, AnyType, xmlNamespace)
            myXmlDocument.PreserveWhitespace = True ' removes CRLF
            myXmlDocument.LoadXml(myStringWriter.ToString)
            Call ExpandEmptyTags(myXmlDocument) ' change <A /> to <A></A>
            Return myXmlDocument

        Catch Ex As Exception
            Return Nothing

        Finally
            xmlSerializer = Nothing
            xmlNamespace = Nothing
            myStringWriter.Dispose()
            myStringWriter = Nothing
            myXmlDocument = Nothing
        End Try

    End Function

End Module
