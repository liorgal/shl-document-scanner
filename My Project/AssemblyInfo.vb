﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("SHL Document Scanner")> 
<Assembly: AssemblyDescription("Warning: This computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted under the maximum extent possible under law.")> 
<Assembly: AssemblyCompany("SHL Telemedicine Ltd")> 
<Assembly: AssemblyProduct("SHL Document Scanner")> 
<Assembly: AssemblyCopyright("Copyright © SHL Telemedicine Ltd 2013")> 
<Assembly: AssemblyTrademark("SHL Telemedicine Ltd 2013")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("334e9cee-0d47-4d70-924b-b5098a3432cb")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.4.0")> 
<Assembly: AssemblyFileVersion("1.0.4.0")> 
